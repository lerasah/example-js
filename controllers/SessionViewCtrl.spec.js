/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:16
*/
describe('SessionViewCtrl Tests With No Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    controller = $controller('SessionViewCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('session object exists', function () {
    expect(scope.session).toEqual(jasmine.any(Object))
  })

  it('redirected to sessions page if no parameter is present', function () {
    expect(pageCtrl.getPath()).toEqual('/sessions')
  })
})

describe('SessionViewCtrl Tests With Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))
  beforeEach(module('ngRoute'))

  var controller, AdminService, pageCtrl, httpBackend, scope, routeParams

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    routeParams = {id: '137'}
    controller = $controller('SessionViewCtrl', {
      $scope: scope,
      Page: Page,
      $routeParams: routeParams
    })
    httpBackend = $httpBackend
    pageCtrl = Page
    // routeParams_prx = $routeParams
  }))

  it('session object exists', function () {
    expect(scope.session).toEqual(jasmine.any(Object))
  })

  it('session_id is set if a valid integer parameter is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('SessionViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'sessions/137').respond({'Code': 200, 'Result': 'Success', 'Data': {'partner_email': 'partner@example.org', 'client_email': 'client@example.org', 'location': '69 Rathnayaka Mawatha, Thalangama South, Pelawatte 12138, Sri Lanka', 'client_gym_name': 'OSMO Fitness', 'status': 'Completed', 'date_time': '2017-07-27T22:53:00.00', 'workout_type': 'Yoga', 'rating_from_client': null, 'client_feedback': null, 'payment': {'session_price': 700, 'partner_payout': 157, 'method': 'VISA (0005)', 'ref_id': '123'}}})
    expect(scope.session_id).toEqual(137)
  })

  it('session_id is set to 0 an invalid session id integer is present', function ($controller) {
    console.log(routeParams)
    routeParams.id = -55.2
    controller = $controller('SessionViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'sessions/137').respond({'Code': 200, 'Result': 'Success', 'Data': {'partner_email': 'partner@example.org', 'client_email': 'client@example.org', 'location': '69 Rathnayaka Mawatha, Thalangama South, Pelawatte 12138, Sri Lanka', 'client_gym_name': 'OSMO Fitness', 'status': 'Completed', 'date_time': '2017-07-27T22:53:00.00', 'workout_type': 'Yoga', 'rating_from_client': null, 'client_feedback': null, 'payment': {'session_price': 700, 'partner_payout': 157, 'method': 'VISA (0005)', 'ref_id': '123'}}})
    expect(scope.session_id).toEqual(137)
  })
})

describe('SessionViewCtrl Tests With invalid Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))
  beforeEach(module('ngRoute'))

  var controller, AdminService, pageCtrl, httpBackend, scope, routeParams

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    routeParams = {id: '-55.2'}
    controller = $controller('SessionViewCtrl', {
      $scope: scope,
      Page: Page,
      $routeParams: routeParams
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('session_id is set to 0 an invalid session id integer is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('SessionViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'sessions/137').respond({'Code': 200, 'Result': 'Success', 'Data': {'partner_email': 'partner@example.org', 'client_email': 'client@example.org', 'location': '69 Rathnayaka Mawatha, Thalangama South, Pelawatte 12138, Sri Lanka', 'client_gym_name': 'OSMO Fitness', 'status': 'Completed', 'date_time': '2017-07-27T22:53:00.00', 'workout_type': 'Yoga', 'rating_from_client': null, 'client_feedback': null, 'payment': {'session_price': 700, 'partner_payout': 157, 'method': 'VISA (0005)', 'ref_id': '123'}}})
    expect(scope.session_id).toEqual(0)
  })
})

/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:08
*/
example.controller('SessionsCtrl', function ($scope, Page, AdminService) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Sessions')

  consolelog('SessionCtrl')

  $scope.hasMoreData = true

  $scope.sortTypes = [
    {
      id: 2,
      text: 'Date (Newest)'
    }, {
      id: 1,
      text: 'Date (Oldest)'
    }, {
      id: 4,
      text: 'Client Name (A-Z)'
    }, {
      id: 3,
      text: 'Partner Name (A-Z)'
    }, {
      id: 5,
      text: 'Session ID'
    }
  ]

  $scope.itemsPerPage = 50

  $scope.searchObject = {
    sort_type: $scope.sortTypes[0].id,
    search_string: '',
    start_index: 0,
    end_index: $scope.itemsPerPage
  }

  $scope.sessions = []

  $scope.searchOnProgress = false
  $scope.executeSearch = function () {
    $scope.searchOnProgress = true
    AdminService.searchSessions(JSON.parse(angular.toJson($scope.searchObject)), function (response) { // eslint-disable-line no-undef
      $scope.searchOnProgress = false
      if (response === false) {
        consolelog('Search Error')
      } else if (Page.checkResponse(response)) {
        consolelog('Search Success')
        if ($scope.searchObject.start_index === 0) {
          consolelog($scope.searchObject.search_string)
          $scope.sessions = []
        }
        $scope.sessions = $scope.sessions.concat(response.data.Data.sessions)
        $scope.searchObject.start_index = response.data.Data.end_index
        $scope.searchObject.end_index = $scope.searchObject.end_index + $scope.itemsPerPage
        $scope.hasMoreData = response.data.Data.sessions.length === $scope.itemsPerPage
      } else if (response.status === 200 && response.data.Code === 201) {
        // no more results
        $scope.hasMoreData = false
      } else {
        consolelog('Search API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
      }
    })
  }
  $scope.executeSearch()

  $scope.scrollTableUp = function () {
    $('.data_sessions_table').animate({ // eslint-disable-line no-undef
      scrollTop: 0
    }, 'fast')
  }

  $scope.keyPressed = function (keyEvent) {
    if (keyEvent.keyCode === 13) {
      $scope.sessions = []
      $scope.searchObject.start_index = 0
      $scope.searchObject.end_index = $scope.itemsPerPage
      $scope.executeSearch()
      $scope.scrollTableUp()
    }
  }

  $scope.changeSort = function (newSortID) {
    if ($scope.searchObject.sort_type === newSortID) {
      return
    }
    $scope.sessions = []
    $scope.searchObject.sort_type = newSortID
    $scope.searchObject.start_index = 0
    $scope.searchObject.end_index = $scope.itemsPerPage
    $scope.executeSearch()
    $scope.scrollTableUp()
  }

  $scope.loadMore = function () {
    if (!$scope.searchOnProgress) {
      $scope.executeSearch()
    }
  }
})

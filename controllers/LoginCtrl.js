/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:41:50
*/
example.controller('LoginCtrl', function ($scope, $cookies, Page, AdminService, md5) { // eslint-disable-line no-undef
  if (Page.isLoggedIn()) {
    Page.redirect('/')
  }

  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setHeader('')
  Page.setTitle('Login')

  consolelog('LoginCtrl')

  $scope.signinObj = {
    username: '',
    pin: '',
    stage: 1
  }

  $scope.submittingForm = false
  $scope.signin = function (valid) {
    if ($scope.signinObj.stage === 1 && valid) {
      // make service call
      $scope.submittingForm = true

      AdminService.signin({
        username: $scope.signinObj.username
      }, function (response) {
        $scope.submittingForm = false
        consolelog(response)
        // consolelog(response.data.Code)
        // consolelog(response.status)
        consolelog(response.status === 200 && response.data.Code === 200)
        if (response === false) {
          Page.commonAlert('An error occured!', 'Oops!')
          return
        }
        if (Page.checkResponse(response)) {
          $scope.signinObj.stage = 2
          consolelog('Stage Changed')
        } else {
          Page.commonAlert(response.data.Data, response.data.Result)
          consolelog('Stage !Changed')
        }
      })
    } else if ($scope.signinObj.stage === 2 && valid) {
      // make service call
      $scope.submittingForm = true
      AdminService.verifyPin({
        username: $scope.signinObj.username,
        pin: $scope.signinObj.pin
      }, function (response) {
        consolelog(response)
        consolelog(response.data.Code)
        consolelog(response.status)
        consolelog(response.status === 200 && response.data.Code === 200)
        $scope.submittingForm = false

        if (response === false) {
          Page.commonAlert('An error occured!', 'Oops!')
          return
        }

        if (Page.checkResponse(response)) {
          var expires = new Date()
          var y = new Date().getFullYear()
          expires.setFullYear(y + 1)
          $cookies.put('token', response.data.Data.Token, {
            'expires': expires
          })
          Page.redirect('/')
        } else {
          Page.commonAlert(response.data.Data, response.data.Result)
          $scope.signinObj.pin = ''
        }
      })
    } else {
      // show error here
    }
  }
})

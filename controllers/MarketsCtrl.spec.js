/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:52
*/
describe('MarketsCtrl Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var goodResponse = {
    'Code': 200,
    'Result': 'Success',
    'Data': {
      'market_types': [
        {
          'id': 1,
          'name': 'Type 1',
          'session_fee': 4000,
          'booking_fee': 99,
          'cancellation_fee': 500,
          'no_gym_fee': 99
        },
        {
          'id': 2,
          'name': 'Type 2',
          'session_fee': 4000,
          'booking_fee': 99,
          'cancellation_fee': 500,
          'no_gym_fee': 99
        },
        {
          'id': 3,
          'name': 'Type 3',
          'session_fee': 100,
          'booking_fee': 200,
          'cancellation_fee': 300,
          'no_gym_fee': 400
        }
      ]
    }
  }

  var badMarketsResponse = {
    'Code': 401,
    'Result': 'Error',
    'Data': {
      Message: 'Unknown Error'
    }
  }

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, ngDialog, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('MarketsCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('Check variables exists', function () {
    expect(scope.loading).toBeDefined()
    expect(scope.markets).toBeDefined()
  })

  it('Good Loading Test', function (done) {
    // 3 markets
    httpBackend.when('GET', apiUrl + 'markets').respond(goodResponse)

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.loadMarkets()
    expect(scope.markets.length).toBe(3)
  })

  it('Load Markets Server Fault', function (done) {
    // 3 markets
    httpBackend.when('GET', apiUrl + 'markets').respond(500, {})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.loadMarkets()
    expect(scope.markets.length).toBe(0)
  })

  it('Load Markets Server Error Reported', function (done) {
    // 3 markets
    httpBackend.when('GET', apiUrl + 'markets').respond(200, badMarketsResponse)

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.loadMarkets()
    expect(scope.markets.length).toBe(0)
  })

  it('Load > Delete', function (done) {
    ngDialog = jasmine.createSpyObj('ngDialog', ['openConfirm'])
    // 3 markets
    httpBackend.when('GET', apiUrl + 'markets').respond(goodResponse)

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.loadMarkets()
    expect(scope.markets.length).toBe(3)
    scope.deleteMarket(scope.markets[0])
  })

  it('Load > Edit Market', function (done) {
    // 3 markets
    httpBackend.when('GET', apiUrl + 'markets').respond(goodResponse)

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.loadMarkets()
    expect(scope.markets.length).toBe(3)
    scope.addEditMarket(scope.markets[0])
  })

  it('Load > Create Market', function (done) {
    // 3 markets
    httpBackend.when('GET', apiUrl + 'markets').respond(goodResponse)

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.loadMarkets()
    expect(scope.markets.length).toBe(3)
    scope.addEditMarket()
  })
})

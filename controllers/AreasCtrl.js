example.controller('AreasCtrl', function ($scope, Page, ngDialog, $filter, AdminService) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Areas')

  consolelog('AreasCtrl')

  $scope.searchString = ''
  $scope.lastSearchString = ''

  $scope.areas = []

  $scope.googleMapsUrl = config[activeConfig].mapsApiURL
  $scope.googleMapsUrlWithKey = config[activeConfig].mapsApiURL + '?key=' + config[activeConfig].mapsApiKey

  $scope.searchOnProgress = false
  $scope.executeSearch = function () {
    $scope.searchOnProgress = true
    AdminService.searchAreas($scope.lastSearchString, function (response) { // eslint-disable-line no-undef
      $scope.searchOnProgress = false
      if (response === false) {
        consolelog('Search Error')
      } else if (Page.checkResponse(response)) {
        consolelog('Search Success')
        $scope.areas = response.data.Data.service_areas
      } else {
        consolelog('Search API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
      }
    })
  }

  $scope.executeSearch()

  $scope.scrollTableUp = function () {
    $('.data_areas_table').animate({ // eslint-disable-line no-undef
      scrollTop: 0
    }, 'fast')
  }

  $scope.keyPressed = function (keyEvent) {
    if (keyEvent.keyCode === 13) {
      $scope.areas = []
      $scope.lastSearchString = $scope.searchString
      $scope.executeSearch()
      $scope.scrollTableUp()
    }
  }

  $scope.milesToMeters = function (miles) {
    return miles * 1609.34
  }

  $scope.deleteArea = function (area) {
    Page.commonConfirm('Are you sure you want to remove \'' + area.location_name + '\' from your list of service areas? You can\'t revert this action.', 'Confirm Removal of Area', 'Confirm', 'Cancel', function (answer) {
      if (answer) {
        AdminService.deleteArea(area.id, function (response) { // eslint-disable-line no-undef
          if (response === false) {
            consolelog('deleteArea Error')
          } else if (Page.checkResponse(response)) {
            consolelog('deleteArea Success')
            $scope.areas = $scope.areas.filter(itArea => itArea.id !== area.id)
            $scope.executeSearch()
          } else {
            consolelog('deleteArea API Returned error')
            Page.commonAlert(response.data.Data, response.data.Result)
          }
        })
      }
    })
  }

  $scope.loadingPopup = false
  $scope.addEditArea = function (area) {
    if ($scope.loadingPopup) {
      return
    }
    $scope.loadingPopup = true
    AdminService.loadMarkets(function (response) { // eslint-disable-line no-undef
      $scope.loadingPopup = false
      if (response === false) {
        consolelog('loadMarkets Error')
      } else if (Page.checkResponse(response)) {
        consolelog('loadMarkets Success')

        if (response.data.Data.market_types.length === 0) {
          Page.commonAlert('No Markets found.', 'Oops!')
          return
        }
        // popup start
        ngDialog.open({
          template: 'partials/areas/area_popup.html',
          className: 'ngdialog-theme-example info editArea',
          showClose: false,
          resolve: {
            area: function () {
              return area !== undefined ? JSON.parse(angular.toJson(area)) : {'id': 0, 'location_name': '', 'location': {'lat': undefined, 'lon': undefined}, 'radius': 1, 'market_id': 0, 'market_name': ''} // eslint-disable-line no-undef
            },
            markets: function () {
              return $filter('orderBy')(response.data.Data.market_types, 'name.toLowerCase()')
            }
          },
          controller: function ($scope, area, markets, Page, AdminService, ngDialog, $timeout) {
            consolelog(area)
            $scope.area = area
            $scope.editMode = area.id === 0
            $scope.toggle = function () {
              $scope.editMode = !$scope.editMode
              $timeout(function () {
                $('input[name=location]').focus()
                $timeout(function () {
                  $('input[name=location]').trigger(jQuery.Event('keydown', { keyCode: 40 }))
                }, 500)
              })
            }
            $scope.markets = markets
            $scope.saving = false
            if (area.id === 0) {
              $scope.area.market_id = markets[0].id
              $scope.area.market_name = markets[0].name
            }

            $scope.save = function (area) {
              // ngDialog.close()
              $scope.saving = true
              AdminService.saveArea(JSON.parse(angular.toJson($scope.area)), function (response) { // eslint-disable-line no-undef
                $scope.saving = false
                if (response === false) {
                  consolelog('Add/Update Area Error')
                } else if (Page.checkResponse(response)) {
                  consolelog('Add/Update Area Success')
                  ngDialog.close()
                } else {
                  consolelog('Add/Update Area Error')
                  Page.commonAlert(response.data.Data, response.data.Result)
                }
              })
            }
          }
        }).closePromise.then(function (dialogResult) {
          consolelog(dialogResult.value)
          $scope.executeSearch()
        })
        // popup end
      } else {
        consolelog('loadMarkets API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
      }
    })
  }
})

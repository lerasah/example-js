/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:28:59
*/
describe('AreasCtrl Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope
  var goodSearchResponse = {
    'Code': 200,
    'Result': 'Success',
    'Data': {
      'service_areas': [
        {
          'id': 1,
          'location_name': 'A1',
          'location': {
            'lat': 30.536912,
            'lon': 98.482946
          },
          'radius': 10,
          'market_name': 'High',
          'created_at': '2017-06-06T20:16:30.96'
        },
        {
          'id': 2,
          'location_name': 'A2',
          'location': {
            'lat': 30.536912,
            'lon': 98.482946
          },
          'radius': 10,
          'market_name': 'High',
          'created_at': '2017-06-06T20:16:30.96'
        }
      ]
    }
  } // 2 areas
  var MarketsgoodResponse = {
    'Code': 200,
    'Result': 'Success',
    'Data': {
      'market_types': [
        {
          'id': 1,
          'name': 'Type 1',
          'session_fee': 4000,
          'booking_fee': 99,
          'cancellation_fee': 500,
          'no_gym_fee': 99
        },
        {
          'id': 2,
          'name': 'Type 2',
          'session_fee': 4000,
          'booking_fee': 99,
          'cancellation_fee': 500,
          'no_gym_fee': 99
        },
        {
          'id': 3,
          'name': 'Type 3',
          'session_fee': 100,
          'booking_fee': 200,
          'cancellation_fee': 300,
          'no_gym_fee': 400
        }
      ]
    }
  }

  var MarketsNoMarkets = {
    'Code': 200,
    'Result': 'Success',
    'Data': {
      'market_types': []
    }
  }

  beforeEach(inject(function ($rootScope, $cookies, Page, ngDialog, $filter, AdminService, md5, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('AreasCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('searchString in correct format', function () {
    expect(scope.searchString).toEqual(jasmine.any(String))
    expect(scope.lastSearchString).toEqual(jasmine.any(String))
  })

  it('Initial Search Test', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(goodSearchResponse)

    done()
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.areas.length).toBe(2)
  })

  it('keyPressed event for enter key', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(goodSearchResponse)
    done()
    httpBackend.flush()
    done()
    scope.searchString = 'TEST'
    scope.keyPressed({keyCode: 13})
    expect(scope.areas.length).toBe(0)
    expect(scope.lastSearchString).toBe(scope.searchString)
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=TEST').respond(goodSearchResponse)
    httpBackend.flush()
    done()
    expect(scope.areas.length).toBe(2)
  })

  it('Seach API Server Error', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(500, {})
    done()
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.areas.length).toBe(0)
  })

  it('Search Returns an API Error (500.etc)', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond({'Code': 401, 'Result': 'Unauthorized', 'Data': {'Message': 'API ERROR'}})
    done()
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.areas.length).toBe(0)
  })

  it('Miles to meters function test', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond({'Code': 401, 'Result': 'Unauthorized', 'Data': {'Message': 'API ERROR'}})
    done()
    httpBackend.flush()

    expect(scope.milesToMeters(1)).toBe(1609.34)
    expect(scope.milesToMeters(100.123456)).toBe(161132.68267904)
  })

  it('Open CREATE Area Popup', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(goodSearchResponse)
    done()
    httpBackend.when('GET', apiUrl + 'markets').respond(MarketsgoodResponse)
    httpBackend.when('GET', 'partials/areas/area_popup.html').respond(200, '<div></div>')
    done()
    scope.addEditArea(scope.areas[0])
    httpBackend.flush()
    expect(scope.loadingPopup).toBe(false)
  })

  it('Open CREATE Area Popup without no markets on the db', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(goodSearchResponse)
    done()
    httpBackend.when('GET', apiUrl + 'markets').respond(MarketsNoMarkets)
    done()
    scope.addEditArea(scope.areas[0])
    httpBackend.flush()
    expect(scope.loadingPopup).toBe(false)
  })

  it('Open CREATE Area Popup when no internet or server failure', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(goodSearchResponse)
    done()
    httpBackend.when('GET', apiUrl + 'markets').respond({'Code': 401, 'Result': 'Unauthorized', 'Data': {'Message': 'API ERROR'}})
    done()
    scope.addEditArea(scope.areas[0])
    httpBackend.flush()
    expect(scope.loadingPopup).toBe(false)
  })

  it('Open DELETE Area Popup', function (done) {
    httpBackend.when('GET', apiUrl + 'serviceArea?searchName=%25').respond(goodSearchResponse)
    done()
    httpBackend.flush()
    // httpBackend.when('GET', 'partials/areas/area_popup.html').respond(200, '<div></div>')
    // done()
    scope.deleteArea(scope.areas[0])
    expect(scope.loadingPopup).toBe(false)
    expect(scope.areas.length).toBe(2)
  })
})

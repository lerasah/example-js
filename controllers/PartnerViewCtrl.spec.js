/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:43:58
*/
describe('PartnerViewCtrl Tests With No Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('partner objects exists', function () {
    expect(scope.partner).toEqual(jasmine.any(Object))
    expect(scope.partnerWorkoutTypes).toEqual(jasmine.any(Object))
  })

  it('redirected to partners page if no parameter is present', function () {
    expect(pageCtrl.getPath()).toEqual('/partners')
  })
})

describe('PartnerViewCtrl Tests With Parameters (id = 773)', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))
  beforeEach(module('ngRoute'))

  var validBasicResult = {'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'Example', 'last_name': 'Partner', 'email': 'partner@example.org', 'phone': '+14548480400', 'dob': '1991-12-27T00:00:00.00', 'date_joined': '2017-07-10T09:33:48.86', 'gym_membership': 'YES', 'gym_location': '123 S Broad St Partners Co, South Broad Street, Philadelphia, PA, USA', 'gym_title': 'OSMO', 'questionnaire_score': '10/10', 'invite_code': '197d1ff0d88c47a', 'invited_code': 'None', 'sessions': {'total': 1033, 'cancelled': 545, 'total_revenue': 257410}}}
  var validWorkoutTypesResult = {'Code': 200, 'Result': 'Success', 'Data': [{'workout_name': 'Back', 'count': 4}, {'workout_name': 'Cardio', 'count': 4}, {'workout_name': 'Chest', 'count': 9}, {'workout_name': 'Circuit Training  ', 'count': 1}, {'workout_name': 'Cycling', 'count': 2}, {'workout_name': 'Legs', 'count': 1}, {'workout_name': 'Meditation ', 'count': 21}, {'workout_name': 'Plyometric', 'count': 1}, {'workout_name': 'Shoulders', 'count': 1}, {'workout_name': 'Stretching', 'count': 57}, {'workout_name': 'Upper Body', 'count': 1}, {'workout_name': 'Yoga', 'count': 516}]}

  var controller, AdminService, pageCtrl, httpBackend, scope, routeParams

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    routeParams = {id: '773'}
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      Page: Page,
      $routeParams: routeParams
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('partner_id is set if a valid integer parameter is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'partner/773/basic').respond(validBasicResult)
    httpBackend.when('GET', apiUrl + 'partner/773/workout-types').respond(validWorkoutTypesResult)
    expect(scope.partner_id).toEqual(773)
  })

  it('partner_id is set if a valid integer parameter is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'partner/263/basic').respond(validBasicResult)
    httpBackend.when('GET', apiUrl + 'partner/773/workout-types').respond(validWorkoutTypesResult)
    expect(scope.partner_id).toEqual(773)
  })

  it('partner_id is set, successfull updates', function ($controller, done) {
    console.log(routeParams)
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    scope.partner_id = 773
    expect(scope.partner_id).toEqual(773)
    httpBackend.when('GET', apiUrl + 'partner/773/basic').respond(validBasicResult)
    httpBackend.when('GET', apiUrl + 'partner/773/workout-types').respond(validWorkoutTypesResult)
    httpBackend.flush()
    scope.refresh()
    expect(scope.partner_id).toEqual(773)
    // update
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('phone', '1234567890')
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('first_name', 'John')
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('dob', new Date())
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('last_name', 'Cena')
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('email', 'someone@example.com')
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('gym_membership', true)

    scope.update('gym_membership', false)
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond(validBasicResult)
    httpBackend.when('GET', apiUrl + 'partner/773/workout-types').respond(validWorkoutTypesResult)
    // httpBackend.flush()
    scope.refresh()
  })

  it('client_id is set, update gym location', function ($controller, done) {
    console.log(routeParams)
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    scope.partner_id = 773
    expect(scope.partner_id).toEqual(773)
    httpBackend.when('GET', apiUrl + 'partner/773/basic').respond(validBasicResult)
    httpBackend.when('GET', apiUrl + 'partner/773/workout-types').respond(validWorkoutTypesResult)
    httpBackend.flush()
    scope.refresh()
    expect(scope.partner_id).toEqual(773)
    // update
    scope.gym.place = {
      geometry: {
        location: {
          lat: function () {
            return 0
          },
          lng: function () {
            return 0
          }
        }
      },
      formatted_address: '123 S Broad St Partners Co, South Broad Street, Philadelphia, PA, USA',
      title: 'OSMO',
      place_id: '1234567890X'
    }
    httpBackend.when('POST', apiUrl + 'partners/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('gym_membership', true)

    scope.update('gym_membership', false)
    httpBackend.when('GET', apiUrl + 'partner/773/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'John', 'last_name': 'Cena', 'email': 'someone@example.com', 'phone': '+11234567890', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'gym_location': '#53, Rathnayaka Mawatha', 'gym_title': 'OSMO', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    httpBackend.when('GET', apiUrl + 'partner/773/workout-types').respond(validWorkoutTypesResult)
    // httpBackend.flush()
    scope.refresh()

    expect(scope.partner.gym_location).toEqual('123 S Broad St Partners Co, South Broad Street, Philadelphia, PA, USA')
    expect(scope.partner.gym_title).toEqual('OSMO')
  })

  it('partner_id is not present in the database', function ($controller) {
    console.log(routeParams)
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    expect(scope.partner_id).toEqual(773)
    httpBackend.when('GET', apiUrl + 'partner/773/basic').respond(401, {'Code': 500, 'Result': 'Error!', 'Data': 'Error!'})
  })
})

describe('PartnerViewCtrl Tests With invalid Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))
  beforeEach(module('ngRoute'))

  var controller, AdminService, pageCtrl, httpBackend, scope, routeParams

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    routeParams = {id: '-55.2'}
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      Page: Page,
      $routeParams: routeParams
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('partner_id is set to 0 an invalid session id integer is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('PartnerViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'partner/773/basic').respond(401, {'Code': 500, 'Result': 'Error!', 'Data': 'Error!'})
    expect(scope.partner_id).toEqual(0)
  })
})

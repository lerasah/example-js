/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:43
*/
describe('LoginCtrl Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('LoginCtrl', {
      $scope: scope,
      // AdminService: AdminService,
      Page: Page
      //
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('signinObj exists', function () {
    expect(scope.signinObj.username).toBeDefined()
    expect(scope.signinObj.pin).toBeDefined()
    expect(scope.signinObj.stage).toBeDefined()
  })

  it('signinObj is initialized', function () {
    expect(scope.signinObj.username).toBe('')
    expect(scope.signinObj.pin).toBe('')
    expect(scope.signinObj.stage).toBe(1)
  })

  it('Signin with a valid username', function (done) {
    scope.signinObj.username = 'coderkk'
    expect(scope.signinObj.username).toBe('coderkk')
    httpBackend.when('POST', apiUrl + 'signin').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    done()
    scope.signin(true)
    // console.log(scope.signinObj);
    httpBackend.flush()

    expect(scope.signinObj.stage).toBe(2)
  })

  it('VerifyPin with valid username and pin', function (done, Page) {
    scope.signinObj.username = 'coderkk'
    scope.signinObj.pin = '1234'
    scope.signinObj.stage = 2
    expect(scope.signinObj.username).toBe('coderkk')
    expect(scope.signinObj.pin).toBe('1234')
    expect(scope.signinObj.stage).toBe(2)
    httpBackend.when('POST', apiUrl + 'verifyPin').respond({'Code': 200, 'Result': 'Success', 'Data': {'Token': '1234567890'}})
    done()
    scope.signin(true)
    console.log(scope.signinObj)
    httpBackend.flush()
    console.log(pageCtrl.isLoggedIn() ? 'Logged In' : 'Not Logged In')
    expect(pageCtrl.isLoggedIn()).toBeTruthy()
  })
})

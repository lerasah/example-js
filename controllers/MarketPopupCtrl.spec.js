/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:46
*/

var existing_market = {
  'id': 1,
  'name': 'Type 1',
  'session_fee': 4000,
  'booking_fee': 99,
  'cancellation_fee': 500,
  'no_gym_fee': 99
}

var new_market = {
  'id': 0,
  'name': '',
  'session_fee': undefined,
  'booking_fee': undefined,
  'cancellation_fee': undefined,
  'no_gym_fee': undefined
}

var badMarketResponse = {
  'Code': 401,
  'Result': 'Error',
  'Data': {
    'Message': 'Unknown Error'
  }
}

var MarketNameExistsResponse = {
  'Code': 401,
  'Result': 'Name Taken',
  'Data': {
    'Message': 'Name Taken'
  }
}

var formObject = {
  '$valid': true,
  'name': {
    '$invalid': false
  }
}

var goodSavedResponse = {'Code': 200, 'Result': 'Success', 'Data': null}
var goodResponse = {
  'Code': 200,
  'Result': 'Success',
  'Data': {
    'market_types': [
      {
        'id': 1,
        'name': 'Type 1',
        'session_fee': 4000,
        'booking_fee': 99,
        'cancellation_fee': 500,
        'no_gym_fee': 99
      },
      {
        'id': 2,
        'name': 'Type 2',
        'session_fee': 4000,
        'booking_fee': 99,
        'cancellation_fee': 500,
        'no_gym_fee': 99
      },
      {
        'id': 3,
        'name': 'Type 3',
        'session_fee': 100,
        'booking_fee': 200,
        'cancellation_fee': 300,
        'no_gym_fee': 400
      }
    ]
  }
}

describe('MarketPopupCtrl Tests - Existing Market', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  beforeEach(function () {
    marketMock = function () {
      return {
        'id': 1,
        'name': 'Type 1',
        'session_fee': 4000,
        'booking_fee': 99,
        'cancellation_fee': 500,
        'no_gym_fee': 99
      }
    }
  })

  var controller, AdminService, pageCtrl, httpBackend, scope, market

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, ngDialog, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('MarketPopupCtrl', {
      $scope: scope,
      Page: Page,
      market: marketMock()
    })
    httpBackend = $httpBackend
    pageCtrl = Page
    market = market
  }))

  it('Check variables defined because its EDIT', function () {
    expect(scope.market.session_fee).toBeDefined()
    expect(scope.market.booking_fee).toBeDefined()
    expect(scope.market.cancellation_fee).toBeDefined()
    expect(scope.market.no_gym_fee).toBeDefined()
    console.info(scope.market)
  })

  it('Save Changes', function (done) {
    // httpBackend.when('POST', apiUrl + 'markets/edit').respond(200, goodSavedResponse)
    // done()
    httpBackend.when('GET', 'partials/markets/market_popup.html').respond(200, '')
    // httpBackend.when('POST', apiUrl + 'markets/edit').respond(200, goodSavedResponse)

    done()
    httpBackend.when('POST', apiUrl + 'markets/edit').respond(200, goodSavedResponse)

    scope.save(scope.market, formObject)
    httpBackend.when('POST', apiUrl + 'markets/edit').respond(200, goodSavedResponse)
    done()
    // httpBackend.when('GET', apiUrl + 'markets').respond(200, goodResponse)
    // done()
    expect(scope.market).toBeDefined()
  })
})

describe('MarketPopupCtrl Tests - New Market', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  beforeEach(function () {
    marketMock = function () {
      return {
        'id': 0,
        'name': 'Type New',
        'session_fee': 4000,
        'booking_fee': 99,
        'cancellation_fee': 500,
        'no_gym_fee': 99
      }
    }
  })

  var controller, AdminService, pageCtrl, httpBackend, scope, market

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, ngDialog, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('MarketPopupCtrl', {
      $scope: scope,
      Page: Page,
      market: marketMock()
    })
    httpBackend = $httpBackend
    pageCtrl = Page
    market = market
  }))

  it('Check variables defined because its EDIT', function () {
    expect(scope.market.session_fee).toBeDefined()
    expect(scope.market.booking_fee).toBeDefined()
    expect(scope.market.cancellation_fee).toBeDefined()
    expect(scope.market.no_gym_fee).toBeDefined()
    console.info(scope.market)
  })

  it('Create', function (done) {
    // httpBackend.when('POST', apiUrl + 'markets/edit').respond(200, goodSavedResponse)
    // done()
    httpBackend.when('GET', 'partials/markets/market_popup.html').respond(200, '')
    // httpBackend.when('POST', apiUrl + 'markets/edit').respond(200, goodSavedResponse)

    httpBackend.when('POST', apiUrl + 'markets').respond(200, goodSavedResponse)
    done()
    scope.save(scope.market, formObject)

    // httpBackend.when('GET', apiUrl + 'markets').respond(200, goodResponse)
    // done()
    expect(scope.market).toBeDefined()
  })
})

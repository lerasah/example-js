example.controller('BaseCtrl', function ($scope, Page) { // eslint-disable-line no-undef
  $scope.page = Page
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Home')

  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  $scope.logout = function () {
    Page.logout()
  }
})

/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:59
*/
example.controller('PartnerViewCtrl', function ($scope, Page, AdminService, $routeParams, $q) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('partner')

  consolelog('PartnerViewCtrl')

  $scope.partner_id = 0

  $scope.partner = {}
  $scope.partnerWorkoutTypes = []
  $scope.gym = {
    'lat': undefined,
    'lon': undefined,
    'Description': undefined,
    'title': undefined,
    'place_id': undefined,
    'place': {}
  }

  $scope.opened = {}

  $scope.open = function ($event, elementOpened) {
    $event.preventDefault()
    $event.stopPropagation()

    $scope.opened[elementOpened] = !$scope.opened[elementOpened]
  }

  consolelog('routeParams')
  consolelog($routeParams)

  $scope.$watch('gym.place', function (newValue, oldValue) {
    if (newValue !== oldValue && newValue !== {}) {
      if ($scope.gym.place.geometry == undefined) {
        return
      }
      $scope.gym.title = newValue.name
      $scope.partner.gym_title = newValue.name
      $scope.partner.gym_location = newValue.formatted_address
      AdminService.editPartner({
        partner_id: $scope.partner_id,
        first_name: null,
        last_name: null,
        email: null,
        phone: null,
        dob: null,
        gym_membership: $scope.partner.gym_membership,
        gym: {
          lat: $scope.gym.place.geometry.location.lat(),
          lon: $scope.gym.place.geometry.location.lng(),
          Description: $scope.gym.place.formatted_address,
          title: $scope.gym.place.name,
          place_id: $scope.gym.place.place_id
        }
      }, function (response) {
        if (response === false) {
          consolelog('editPartner Error')
          d.reject('Error!')
        } else if (Page.checkResponse(response)) {
          consolelog('editPartner Success')
        } else if (response.status === 200 && response.data.Code === 401) {
          // no data
          Page.commonAlert(response.data.Data.Message, response.data.Result)
          Page.redirect('/login')
        } else if (response.status === 200 && response.data.Code === 402) {
          // phone number taken
          Page.commonAlert(response.data.Data, response.data.Result)
        } else {
          consolelog('editPartner API Returned error')
          Page.commonAlert(response.data.Data.Message, response.data.Result)
        }
      })
    }
  }, true)

  if ($routeParams.id !== undefined) {
    var paramPartner = Math.floor(Number(($routeParams.id)))
    if (String(paramPartner) === $routeParams.id && paramPartner > 0) {
      $scope.partner_id = Math.floor(Number(($routeParams.id)))
    } else {
      Page.commonAlert('Invalid URL')
      Page.redirect('/partners')
    }
  } else {
    Page.commonAlert('Invalid URL')
    Page.redirect('/partners')
  }

  $scope.update = function (fieldName, fieldValue) {
    var d = $q.defer()

    if (fieldName === 'phone') {
      fieldValue = fieldValue.replace(/\D/g, '')
      if (fieldValue.length !== 10) {
        d.reject('Phone number must be at 10 digits!')
        return d.promise
      }
    }

    switch (fieldName) {
      case 'first_name':
      case 'last_name':
      case 'email':
      case 'phone':
        if (fieldValue.trim().length === 0) {
          d.reject('Field cannot be blank!')
          return d.promise
        }
        break
      case 'dob':
        if (typeof (fieldValue) !== 'object') {
          d.reject('Invalid value detected')
          return d.promise
        }
        if ((new Date().getTime()) < fieldValue.getTime()) {
          d.reject('Cannot be a future date')
          return d.promise
        }
        break
      case 'gym_membership':
        if (typeof (fieldValue) !== 'boolean') {
          d.reject('Invalid value detected')
          return d.promise
        }
        if (fieldValue) {
          d.resolve()
          return d.promise
        }
        break
      default:
    }

    AdminService.editPartner({
      partner_id: $scope.partner_id,
      first_name: fieldName === 'first_name' ? fieldValue : null,
      last_name: fieldName === 'last_name' ? fieldValue : null,
      email: fieldName === 'email' ? fieldValue : null,
      phone: fieldName === 'phone' ? '+1' + fieldValue : null,
      dob: fieldName === 'dob' ? fieldValue.toJSON().slice(0, 10) + 'T00:00:00.000' : null,
      gym_membership: fieldName === 'gym_membership' ? fieldValue : $scope.partner.gym_membership,
      gym: null
    }, function (response) {
      if (response === false) {
        consolelog('editPartner Error')
        d.reject('Error!')
      } else if (Page.checkResponse(response)) {
        consolelog('editPartner Success')
        d.resolve()
      } else if (response.status === 200 && response.data.Code === 401) {
        // no data
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        d.reject(response.data.Data.Message || response.data.Result)
        Page.redirect('/login')
      } else if (response.status === 200 && (response.data.Code === 402 || response.data.Code === 403)) {
        // phone number taken
        // Page.commonAlert(response.data.Data, response.data.Result)
        d.reject(response.data.Data || response.data.Result)
      } else {
        consolelog('editPartner API Returned error')
        // Page.commonAlert(response.data.Data.Message, response.data.Result)
        d.reject(response.data.Data.Message || response.data.Result)
      }
    })
    return d.promise
  }

  $scope.refresh = function () {
    AdminService.getPartner($scope.partner_id, function (response) {
      if (response === false) {
        consolelog('getpartner Error')
      } else if (Page.checkResponse(response)) {
        consolelog('getpartner Success')
        $scope.partner = response.data.Data
        $scope.partner.dob2 = response.data.Data.dob === '0001-01-01T00:00:00.00' ? undefined : new Date(response.data.Data.dob)
        if ($scope.partner.phone == null) {
          $scope.partner.phone = ''
        }
        $scope.partner.phone = $scope.partner.phone.trim().substr($scope.partner.phone.trim().length - 10)
        $scope.partner.gym_membership = $scope.partner.gym_membership === 'YES'
        $scope.gym = {
          'lat': undefined,
          'lon': undefined,
          'Description': $scope.partner.gym_location != null ? $scope.partner.gym_location : undefined,
          'title': $scope.partner.gym_title != null ? $scope.partner.gym_title : undefined,
          'place_id': undefined,
          'place': {}
        }
      } else if (response.status === 200 && response.data.Code === 401) {
        // no data
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        Page.redirect('/partners')
      } else {
        consolelog('getpartner API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        Page.redirect('/partners')
      }
    })
    AdminService.getPartnerWorkoutTypes($scope.partner_id, function (response) {
      if (response === false) {
        consolelog('getPartnerWorkoutTypes Error')
      } else if (Page.checkResponse(response)) {
        consolelog('getPartnerWorkoutTypes Success')
        $scope.partnerWorkoutTypes = response.data.Data
      }
    })
  }

  if ($scope.partner_id !== 0) {
    Page.setTitle('Partner ' + $scope.partner_id)
    $scope.refresh()
  }
})

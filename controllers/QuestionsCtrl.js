/*
* @Author: sachethana
* @Date:   2018-02-27 10:05:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:44:01
*/
example.controller('QuestionsCtrl', function ($scope, Page, AdminService, ngDialog) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Questionnaire')

  consolelog('QuestionsCtrl')

  $scope.searchString = ''
  $scope.lastSearchString = ''

  $scope.questions = []

  $scope.loading = false
  $scope.refreshQuestions = function () {
    $scope.loading = true
    AdminService.getQuestions(function (response) { // eslint-disable-line no-undef
      $scope.loading = false
      if (response === false) {
        consolelog('refreshQuestions Error')
      } else if (Page.checkResponse(response)) {
        consolelog('refreshQuestions Success')
        $scope.questions = response.data.Data.Questions
      } else {
        consolelog('refreshQuestions API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
      }
    })
  }

  $scope.refreshQuestions()

  $scope.addEditQuestion = function (question) {
    ngDialog.open({
      template: 'partials/questions/question_popup.html',
      className: 'ngdialog-theme-example info addQuestion',
      showClose: false,
      resolve: {
        question: function () {
          return question !== undefined ? JSON.parse(angular.toJson(question)) : {id: 0, question: '', correct_answer: 0, partner_type: 3, answer_1: '', answer_2: '', answer_3: '', answer_4: ''} // eslint-disable-line no-undef
        }
      },
      controller: function ($scope, question, Page, AdminService, ngDialog) {
        consolelog(question)
        $scope.question = question
        $scope.saving = false
        $scope.save = function () {
          if ($scope.question.correct_answer === 0) {
            Page.commonAlert('Please mark the correct answer!', 'Oops!')
            return
          }
          $scope.saving = true
          AdminService.saveQuestion(JSON.parse(angular.toJson($scope.question)), function (response) { // eslint-disable-line no-undef
            $scope.saving = false
            if (response === false) {
              consolelog('Add/Update question Error')
            } else if (Page.checkResponse(response)) {
              consolelog('Add/Update question Success')
              ngDialog.close()
            } else {
              consolelog('Add/Update question Error')
              Page.commonAlert(response.data.Data, response.data.Result)
            }
          })
        }
      }
    }).closePromise.then(function (dialogResult) {
      consolelog(dialogResult.value)
      $scope.refreshQuestions()
    })
  }

  $scope.deleteQuestion = function (question) {
    Page.commonConfirm('Are you sure you want to remove \'' + question.question + '\' from your list of questions? You can\'t revert this action.', 'Confirm Removal of Question', 'Confirm', 'Cancel', function (answer) {
      if (answer) {
        AdminService.deleteQuestion(question.id, function (response) { // eslint-disable-line no-undef
          if (response === false) {
            consolelog('deleteQuestion Error')
          } else if (Page.checkResponse(response)) {
            consolelog('deleteQuestion Success')
            $scope.questions = $scope.questions.filter(obj => obj.id !== question.id)
            $scope.refreshQuestions()
          } else {
            consolelog('deleteQuestion API Returned error')
            Page.commonAlert(response.data.Data, response.data.Result)
          }
        })
      }
    })
  }
})

/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:20
*/
example.controller('ClientsCtrl', function ($scope, Page, AdminService) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Clients')

  consolelog('ClientsCtrl')

  $scope.hasMoreData = true

  $scope.sortTypes = [
    {
      id: 2,
      text: 'Date (Newest)'
    }, {
      id: 1,
      text: 'Date (Oldest)'
    }, {
      id: 3,
      text: 'Client Name (A-Z)'
    }, {
      id: 4,
      text: 'Client ID'
    }
  ]

  $scope.itemsPerPage = 50

  $scope.searchObject = {
    sort_type: $scope.sortTypes[0].id,
    search_string: '',
    start_index: 0,
    end_index: $scope.itemsPerPage
  }

  $scope.clients = []

  $scope.searchOnProgress = false
  $scope.executeSearch = function () {
    $scope.searchOnProgress = true
    AdminService.searchClients(JSON.parse(angular.toJson($scope.searchObject)), function (response) { // eslint-disable-line no-undef
      $scope.searchOnProgress = false
      if (response === false) {
        consolelog('Search Error')
      } else if (Page.checkResponse(response)) {
        consolelog('Search Success')
        if ($scope.searchObject.start_index === 0) {
          consolelog($scope.searchObject.search_string)
          $scope.clients = []
        }
        $scope.clients = $scope.clients.concat(response.data.Data.clients)
        $scope.searchObject.start_index = response.data.Data.end_index
        $scope.searchObject.end_index = $scope.searchObject.end_index + $scope.itemsPerPage
        $scope.hasMoreData = response.data.Data.clients.length === $scope.itemsPerPage
      } else if (response.status === 200 && response.data.Code === 201) {
        // no more results
        $scope.hasMoreData = false
      } else {
        consolelog('Search API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
      }
    })
  }
  $scope.executeSearch()

  $scope.scrollTableUp = function () {
    $('.data_clients_table').animate({
      scrollTop: 0
    }, 'fast')
  }

  $scope.keyPressed = function (keyEvent) {
    if (keyEvent.keyCode === 13) {
      $scope.clients = []
      $scope.searchObject.start_index = 0
      $scope.searchObject.end_index = $scope.itemsPerPage
      $scope.executeSearch()
      $scope.scrollTableUp()
    }
  }

  $scope.changeSort = function (newSortID) {
    if ($scope.searchObject.sort_type === newSortID) {
      return
    }
    $scope.clients = []
    $scope.searchObject.sort_type = newSortID
    $scope.searchObject.start_index = 0
    $scope.searchObject.end_index = $scope.itemsPerPage
    $scope.executeSearch()
    $scope.scrollTableUp()
  }

  $scope.loadMore = function () {
    if (!$scope.searchOnProgress) {
      $scope.executeSearch()
    }
  }
})

/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:14
*/
example.controller('SessionViewCtrl', function ($scope, Page, AdminService, $routeParams) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Session')

  consolelog('SessionViewCtrl')

  $scope.session_id = 0

  $scope.session = {}

  consolelog('routeParams')
  consolelog($routeParams)

  if ($routeParams.id !== undefined) {
    var param = Math.floor(Number(($routeParams.id)))
    if (String(param) === $routeParams.id && param > 0) {
      $scope.session_id = Math.floor(Number(($routeParams.id)))
    } else {
      Page.commonAlert('Invalid URL')
      Page.redirect('/sessions')
    }
  } else {
    Page.commonAlert('Invalid URL')
    Page.redirect('/sessions')
  }

  if ($scope.session_id !== 0) {
    Page.setTitle('Session ' + $scope.session_id)
    AdminService.getSession($scope.session_id, function (response) {
      if (response === false) {
        consolelog('getSession Error')
      } else if (Page.checkResponse(response)) {
        consolelog('getSession Success')
        $scope.session = response.data.Data
      } else if (response.status === 200 && response.data.Code === 401) {
        // no data
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        Page.redirect('/sessions')
      } else {
        consolelog('getSession API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        Page.redirect('/sessions')
      }
    })
  }
})

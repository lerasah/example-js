/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:33
*/
describe('ClientViewCtrl Tests With No Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('client object exists', function () {
    expect(scope.client).toEqual(jasmine.any(Object))
  })

  it('redirected to clients page if no parameter is present', function () {
    expect(pageCtrl.getPath()).toEqual('/clients')
  })
})

describe('ClientViewCtrl Tests With Parameters (id = 263)', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))
  beforeEach(module('ngRoute'))

  var controller, AdminService, pageCtrl, httpBackend, scope, routeParams

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    routeParams = {id: '263'}
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      Page: Page,
      $routeParams: routeParams
    })
    httpBackend = $httpBackend
    pageCtrl = Page
    // routeParams_prx = $routeParams
  }))

  it('client object exists', function () {
    expect(scope.client).toEqual(jasmine.any(Object))
  })

  it('client_id is set if a valid integer parameter is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'Sumi', 'last_name': 'Ekanayake', 'email': 'client@example.org', 'phone': '+15554364436', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    expect(scope.client_id).toEqual(263)
  })

  it('client_id is not present in the database', function ($controller) {
    console.log(routeParams)
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond(401, {'Code': 500, 'Result': 'Error!', 'Data': 'Error!'})
    expect(scope.client_id).toEqual(263)
  })

  it('client_id is set, successfull updates', function ($controller, done) {
    console.log(routeParams)
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    scope.client_id = 263
    expect(scope.client_id).toEqual(263)
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'Sumi', 'last_name': 'Ekanayake', 'email': 'client@example.org', 'phone': '+15554364436', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'gym_location': '53, Rathnayaka Mw', 'gym_title': 'OSMO', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    httpBackend.flush()
    scope.refresh()
    expect(scope.client_id).toEqual(263)
    // update
    httpBackend.when('POST', apiUrl + 'clients/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('phone', '1234567890')
    httpBackend.when('POST', apiUrl + 'clients/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('first_name', 'John')
    httpBackend.when('POST', apiUrl + 'clients/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('last_name', 'Cena')
    httpBackend.when('POST', apiUrl + 'clients/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('email', 'someone@example.com')
    httpBackend.when('POST', apiUrl + 'clients/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('gym_membership', true)

    scope.update('gym_membership', false)
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'John', 'last_name': 'Cena', 'email': 'someone@example.com', 'phone': '+11234567890', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'gym_location': '53, Rathnayaka Mw', 'gym_title': 'OSMO', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    // httpBackend.flush()
    scope.refresh()

    // expect(scope.client.first_name).toEqual('John')
    // expect(scope.client.last_name).toEqual('Cena')
    // expect(scope.client.email).toEqual('someone@example.com')
    // expect(scope.client.phone).toEqual('1234567890')
  })

  it('client_id is set, update gym location', function ($controller, done) {
    console.log(routeParams)
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    scope.client_id = 263
    expect(scope.client_id).toEqual(263)
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'Sumi', 'last_name': 'Ekanayake', 'email': 'client@example.org', 'phone': '+15554364436', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'gym_location': '#53, Rathnayaka Mawatha', 'gym_title': 'OSMO', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    httpBackend.flush()
    scope.refresh()
    expect(scope.client_id).toEqual(263)
    // update
    scope.gym.place = {
      geometry: {
        location: {
          lat: function () {
            return 0
          },
          lng: function () {
            return 0
          }
        }
      },
      formatted_address: '#53, Rathnayaka Mawatha',
      title: 'OSMO',
      place_id: '1234567890X'
    }
    httpBackend.when('POST', apiUrl + 'clients/edit').respond({'Code': 200, 'Result': 'Success', 'Data': ''})
    httpBackend.flush()
    scope.update('gym_membership', true)

    scope.update('gym_membership', false)
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'John', 'last_name': 'Cena', 'email': 'someone@example.com', 'phone': '+11234567890', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'gym_location': '#53, Rathnayaka Mawatha', 'gym_title': 'OSMO', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    // httpBackend.flush()
    scope.refresh()

    expect(scope.client.gym_location).toEqual('#53, Rathnayaka Mawatha')
    expect(scope.client.gym_title).toEqual('OSMO')
  })
})

describe('ClientViewCtrl Tests With invalid Parameters', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))
  beforeEach(module('ngRoute'))

  var controller, AdminService, pageCtrl, httpBackend, scope, routeParams

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend, $routeParams) {
    scope = $rootScope.$new()
    routeParams = {id: '-55.2'}
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      Page: Page,
      $routeParams: routeParams
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('client_id is set to 0 an invalid session id integer is present', function ($controller) {
    console.log(routeParams)
    controller = $controller('ClientViewCtrl', {
      $scope: scope,
      $routeParams: routeParams
    })
    httpBackend.when('GET', apiUrl + 'client/263/basic').respond({'Code': 200, 'Result': 'Success', 'Data': {'first_name': 'Sumi', 'last_name': 'Ekanayake', 'email': 'client@example.org', 'phone': '+15554364436', 'status': 'ACTIVE', 'active_time': '226 days', 'date_joined': '2017-07-10T09:46:11.61', 'gym_membership': 'YES', 'avg_rating': 1.0, 'sessions': {'total': 1093, 'cancelled': 588, 'total_expenditure': '890065'}}})
    expect(scope.client_id).toEqual(0)
  })
})

/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:43:56
*/
example.controller('MarketsCtrl', function ($scope, Page, AdminService, ngDialog) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Markets')

  consolelog('MarketsCtrl')

  $scope.markets = []

  $scope.loading = false
  $scope.loadMarkets = function () {
    $scope.loading = true
    AdminService.loadMarkets(function (response) { // eslint-disable-line no-undef
      $scope.loading = false
      if (response === false) {
        consolelog('loadMarkets Error')
      } else if (Page.checkResponse(response)) {
        consolelog('loadMarkets Success')
        $scope.markets = response.data.Data.market_types
      } else {
        consolelog('loadMarkets API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
      }
    })
  }
  $scope.loadMarkets()

  $scope.addEditMarket = function (market) {
    ngDialog.open({
      template: 'partials/markets/market_popup.html',
      className: 'ngdialog-theme-example info createMarket',
      showClose: false,
      resolve: {
        market: function () {
          return market !== undefined ? JSON.parse(angular.toJson(market)) : {'id': 0, 'name': '', 'session_fee': undefined, 'booking_fee': undefined, 'cancellation_fee': undefined, 'no_gym_fee': undefined} // eslint-disable-line no-undef
        }
      },
      controller: 'MarketPopupCtrl'
    }).closePromise.then(function (dialogResult) {
      consolelog(dialogResult.value)
      $scope.loadMarkets()
    })
  }

  $scope.deleteMarket = function (market) {
    Page.commonConfirm('Are you sure you want to remove \'' + market.name + '\' from your list of markets? You can\'t revert this action. Removing a market type will remove all Service Areas that are tied to it.', 'Confirm Removal of Market', 'Confirm', 'Cancel', function (answer) {
      if (answer) {
        AdminService.deleteMarket(market.id, function (response) { // eslint-disable-line no-undef
          if (response === false) {
            consolelog('deleteMarket Error')
          } else if (Page.checkResponse(response)) {
            consolelog('deleteMarket Success')
            $scope.markets = $scope.markets.filter(it => it.id !== market.id)
            $scope.loadMarkets()
          } else {
            consolelog('deleteMarket API Returned error')
            Page.commonAlert(response.data.Data, response.data.Result)
          }
        })
      }
    })
  }
})

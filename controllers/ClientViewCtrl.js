/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:29
*/
example.controller('ClientViewCtrl', function ($scope, Page, AdminService, $routeParams, $q) { // eslint-disable-line no-undef
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  }

  Page.setHeader('partials/header.html')
  Page.setBodyClass('partners sub-page-menu-empty')
  Page.setTitle('Client')

  consolelog('ClientViewCtrl')

  $scope.client_id = 0

  $scope.client = {}
  $scope.gym = {
    'lat': undefined,
    'lon': undefined,
    'Description': undefined,
    'title': undefined,
    'place_id': undefined,
    'place': {}
  }

  $scope.$watch('gym.place', function (newValue, oldValue) {
    if (newValue !== oldValue && newValue !== {}) {
      if ($scope.gym.place.geometry == undefined) {
        return
      }
      $scope.gym.title = newValue.name
      $scope.client.gym_title = newValue.name
      $scope.client.gym_location = newValue.formatted_address
      AdminService.editClient({
        client_id: $scope.client_id,
        first_name: null,
        last_name: null,
        email: null,
        phone: null,
        gym_membership: $scope.client.gym_membership,
        gym: {
          lat: $scope.gym.place.geometry.location.lat(),
          lon: $scope.gym.place.geometry.location.lng(),
          Description: $scope.gym.place.formatted_address,
          title: $scope.gym.place.name,
          place_id: $scope.gym.place.place_id
        }
      }, function (response) {
        if (response === false) {
          consolelog('editClient Error')
          d.reject('Error!')
        } else if (Page.checkResponse(response)) {
          consolelog('editClient Success')
        } else if (response.status === 200 && response.data.Code === 401) {
          // no data
          Page.commonAlert(response.data.Data.Message, response.data.Result)
          Page.redirect('/login')
        } else if (response.status === 200 && response.data.Code === 402) {
          // phone number taken
          Page.commonAlert(response.data.Data, response.data.Result)
        } else {
          consolelog('editClient API Returned error')
          Page.commonAlert(response.data.Data.Message, response.data.Result)
        }
      })
    }
  }, true)

  consolelog('routeParams')
  consolelog($routeParams)

  if ($routeParams.id !== undefined) {
    var paramClient = Math.floor(Number(($routeParams.id)))
    if (String(paramClient) === $routeParams.id && paramClient > 0) {
      $scope.client_id = Math.floor(Number(($routeParams.id)))
    } else {
      Page.commonAlert('Invalid URL')
      Page.redirect('/clients')
    }
  } else {
    Page.commonAlert('Invalid URL')
    Page.redirect('/clients')
  }

  $scope.update = function (fieldName, fieldValue) {
    var d = $q.defer()

    if (fieldName === 'phone') {
      fieldValue = fieldValue.replace(/\D/g, '')
      if (fieldValue.length !== 10) {
        d.reject('Phone number must be at 10 digits!')
        return d.promise
      }
    }

    switch (fieldName) {
      case 'first_name':
      case 'last_name':
      case 'email':
      case 'phone':
        if (fieldValue.trim().length === 0) {
          d.reject('Field cannot be blank!')
          return d.promise
        }
        break
      case 'gym_membership':
        if (typeof (fieldValue) !== 'boolean') {
          d.reject('Invalid value detected')
          return d.promise
        }
        if (fieldValue) {
          d.resolve()
          return d.promise
        }
        break
    }

    AdminService.editClient({
      client_id: $scope.client_id,
      first_name: fieldName === 'first_name' ? fieldValue : null,
      last_name: fieldName === 'last_name' ? fieldValue : null,
      email: fieldName === 'email' ? fieldValue : null,
      phone: fieldName === 'phone' ? '+1' + fieldValue : null,
      gym_membership: fieldName === 'gym_membership' ? fieldValue : $scope.client.gym_membership,
      gym: null
    }, function (response) {
      if (response === false) {
        consolelog('editClient Error')
        d.reject('Error!')
      } else if (Page.checkResponse(response)) {
        consolelog('editClient Success')
        d.resolve()
      } else if (response.status === 200 && response.data.Code === 401) {
        // no data
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        d.reject(response.data.Data.Message || response.data.Result)
        Page.redirect('/login')
      } else if (response.status === 200 && response.data.Code === 402) {
        // phone number taken
        // Page.commonAlert(response.data.Data, response.data.Result)
        d.reject(response.data.Data || response.data.Result)
      } else {
        consolelog('editClient API Returned error')
        // Page.commonAlert(response.data.Data.Message, response.data.Result)
        d.reject(response.data.Data.Message || response.data.Result)
      }
    })
    return d.promise
  }

  $scope.refresh = function () {
    AdminService.getClient($scope.client_id, function (response) {
      if (response === false) {
        consolelog('getClient Error')
      } else if (Page.checkResponse(response)) {
        consolelog('getClient Success')
        $scope.client = response.data.Data
        if ($scope.client.phone == null) {
          $scope.client.phone = ''
        }
        $scope.client.phone = $scope.client.phone.trim().substr($scope.client.phone.trim().length - 10)
        $scope.client.gym_membership = $scope.client.gym_membership === 'YES'
        $scope.gym = {
          'lat': undefined,
          'lon': undefined,
          'Description': $scope.client.gym_location != null ? $scope.client.gym_location : undefined,
          'title': $scope.client.gym_title != null ? $scope.client.gym_title : undefined,
          'place_id': undefined,
          'place': {}
        }
      } else if (response.status === 200 && response.data.Code === 401) {
        // no data
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        Page.redirect('/clients')
      } else {
        consolelog('getClient API Returned error')
        Page.commonAlert(response.data.Data.Message, response.data.Result)
        Page.redirect('/clients')
      }
    })
  }

  if ($scope.client_id !== 0) {
    Page.setTitle('Client ' + $scope.client_id)
    $scope.refresh()
  }
})

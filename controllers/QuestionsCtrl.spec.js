/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:06
*/
describe('QuestionsCtrl Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope
  // 10 answers
  var goodRefreshResponse = {'Code': 200, 'Result': 'Success', 'Data': {'Questions': [{'id': 52, 'questions': 'If I had an appointment set with a client and suddenly had other plans arise, I would…', 'answer_1': 'Follow through with my appointment. I value being a reliable partner.', 'answer_2': 'It depends on the client/plans.', 'answer_3': 'Cancel the training session. I can get another client anytime.', 'answer_4': 'Check to see if the client truly wants to have the training session.', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 53, 'questions': 'Before beginning an initial training session with a new client, I would…', 'answer_1': 'Chat and get to know their personality.', 'answer_2': 'Ask questions to understand their goals and previous workout experience.', 'answer_3': 'Ensure that they have no previous health problems or contraindications.', 'answer_4': 'All of the above', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 54, 'questions': 'My client is overweight and not comfortable using the machines in a gym setting. To ensure their comfort and success, I would...', 'answer_1': 'Use bodyweight and resistance bands. Resistance training is imperative to           \nweight loss.', 'answer_2': 'Suggest that they just use the treadmill. They don’t need to lift weights anyway.', 'answer_3': 'Push them to use the machines anyway. They need to overcome their fear.', 'answer_4': 'Running, running, and more running.', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 55, 'questions': 'A female client who has no past experience in a gym wants to tone her arms and midsection, but does not want to lift weights in fear of getting “bulky”. I would…', 'answer_1': 'Write her a program with lots of weight lifting, regardless of what she says.', 'answer_2': 'Present her with information and photos of females who lift weights to correct \nher perception.', 'answer_3': 'Respect her wishes and avoid lifting weights even though she may not reach her \ngoals.', 'answer_4': 'Disagree with her and tell her she lacks knowledge.', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 56, 'questions': 'My client is in a rush and only has 30 minutes to workout. I would…', 'answer_1': 'Skip the warm-up and cool down to allow for a longer workout.', 'answer_2': 'Include the warm-up and cool down, but incorporate a more time efficient \nworkout (i.ee. HIIT)', 'answer_3': 'Dismiss them and tell them we will meet again when they have more time.', 'answer_4': 'Disregard the warm-up and go straight into heavy weight training.', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 57, 'questions': 'Which of the following is not appropriate for a health and fitness professional to perform during an assessment?', 'answer_1': 'Screen for exercise limitations.', 'answer_2': 'Obtain exercise guidelines from qualified medical practitioner.', 'answer_3': 'Diagnose a medical condition.', 'answer_4': 'Refer clients to a qualified medical practitioner.', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 58, 'questions': 'When designing a flexibility training session, how much volume (in time) should be included per muscle group?', 'answer_1': '90 seconds', 'answer_2': '60 seconds', 'answer_3': '30 seconds', 'answer_4': '120 seconds', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 59, 'questions': 'If your client were looking to increase their muscle size, what would their workouts consist of?', 'answer_1': 'High repetitions, low weight', 'answer_2': 'Moderate repetitions, moderate weight', 'answer_3': 'Low repetitions, high weight', 'answer_4': 'Fast paced, cardio driven, with resistance training only', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 60, 'questions': 'If your client were looking to increase their muscular endurance, what would their workouts consist of?', 'answer_1': 'High repetitions, low weight', 'answer_2': 'Moderate repetitions, moderate weight', 'answer_3': 'Low repetitions, high weight', 'answer_4': 'Slow paced, no cardio, high weight training only', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}, {'id': 61, 'questions': 'Imagine a client is discouraged during a training session. I…', 'answer_1': 'End the session and tell them to come back when they’re feeling better.', 'answer_2': 'Sit them down and have a chat to encourage them to continue the workout.', 'answer_3': 'Push them even harder. No excuses.', 'answer_4': 'Tell them to quit and go home.', 'correct_answer': 1, 'partner_type': 'ENTHUSIAST'}]}}

  beforeEach(inject(function ($rootScope, $cookies, Page, ngDialog, $filter, AdminService, md5, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('QuestionsCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('Initial Load Test', function (done) {
    httpBackend.when('GET', apiUrl + 'questions').respond(goodRefreshResponse)

    done()
    httpBackend.flush()
    scope.refreshQuestions()
    expect(scope.questions.length).toBe(10)
  })

  it('Questions API Server Error', function (done) {
    httpBackend.when('GET', apiUrl + 'questions').respond(500, {})
    done()
    httpBackend.flush()
    scope.refreshQuestions()
    expect(scope.questions.length).toBe(0)
  })

  it('/questions Returns an API Error (500.etc)', function (done) {
    httpBackend.when('GET', apiUrl + 'questions').respond({'Code': 401, 'Result': 'Unauthorized', 'Data': {'Message': 'API ERROR'}})
    done()
    httpBackend.flush()
    scope.refreshQuestions()
    expect(scope.questions.length).toBe(0)
  })

  it('Open Delete Question Popup', function (done) {
    httpBackend.when('GET', apiUrl + 'questions').respond(goodRefreshResponse)

    done()
    httpBackend.flush()
    scope.refreshQuestions()
    expect(scope.questions.length).not.toBe(0)
    scope.deleteQuestion(scope.questions[0])
  })

  it('Open Add Question Popup', function (done) {
    httpBackend.when('GET', apiUrl + 'questions').respond(goodRefreshResponse)

    done()
    httpBackend.flush()
    scope.refreshQuestions()
    expect(scope.questions.length).not.toBe(0)
    scope.addEditQuestion()
  })

  it('Open Edit Question Popup', function (done) {
    httpBackend.when('GET', apiUrl + 'questions').respond(goodRefreshResponse)

    done()
    httpBackend.flush()
    scope.refreshQuestions()
    expect(scope.questions.length).not.toBe(0)
    scope.addEditQuestion(scope.questions[0])
  })
})

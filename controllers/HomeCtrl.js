/*
* @Author: sachethana
* @Date:   2018-02-16 12:23:08
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:36
*/
example.controller('HomeCtrl', function ($scope, $cookies, Page, AdminService, md5) { // eslint-disable-line no-undef
  Page.setHeader('')
  Page.setTitle('Home')

  consolelog('HomeCtrl')
  if (!Page.isLoggedIn()) {
    Page.redirect('/login')
  } else {
    Page.redirect('/sessions')
  }
})

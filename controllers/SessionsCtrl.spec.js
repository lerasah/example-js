/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:11
*/
describe('SessionsCtrl Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('SessionsCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('sortTypes exists', function () {
    expect(scope.sortTypes).toBeDefined()
  })

  it('flags exists', function () {
    expect(scope.hasMoreData).toBeDefined()
    expect(scope.itemsPerPage).toBeDefined()
    expect(scope.searchOnProgress).toBeDefined()
  })

  it('searchObject in correct format', function () {
    expect(scope.searchObject.sort_type).toEqual(jasmine.any(Number))
    expect(scope.searchObject.start_index).toEqual(jasmine.any(Number))
    expect(scope.searchObject.end_index).toEqual(jasmine.any(Number))
    expect(scope.searchObject.search_string).toEqual(jasmine.any(String))
  })

  it('Initial Search Test', function (done) {
    httpBackend.when('POST', apiUrl + 'sessions').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'sessions': [{'session_id': 24, 'partner_name': 'chats vi', 'client_name': 'Ggfd Bbdh', 'date': '2017-06-07T00:00:30.96'}]}})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(49)
    expect(scope.searchObject.end_index).toBe(100)
  })

  it('keyPressed event for enter key', function (done) {
    httpBackend.when('POST', apiUrl + 'sessions').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'sessions': [{'session_id': 24, 'partner_name': 'chats vi', 'client_name': 'Ggfd Bbdh', 'date': '2017-06-07T00:00:30.96'}]}})
    httpBackend.flush()
    done()
    scope.keyPressed({keyCode: 13})
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
  })

  it('changeSort changes the search object sort_type', function (done) {
    httpBackend.when('POST', apiUrl + 'sessions').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'sessions': [{'session_id': 24, 'partner_name': 'chats vi', 'client_name': 'Ggfd Bbdh', 'date': '2017-06-07T00:00:30.96'}]}})
    httpBackend.flush()
    done()
    expect(scope.searchObject.sort_type).toBe(2)
    scope.changeSort(5)
    expect(scope.searchObject.sort_type).toBe(5)
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
  })

  it('ensure loadMore searches again', function (done) {
    httpBackend.when('POST', apiUrl + 'sessions').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'sessions': [{'session_id': 24, 'partner_name': 'chats vi', 'client_name': 'Ggfd Bbdh', 'date': '2017-06-07T00:00:30.96'}]}})
    httpBackend.flush()
    done()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(49)
    expect(scope.searchObject.end_index).toBe(100)
    httpBackend.when('POST', apiUrl + 'sessions').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 50, 'end_index': 100, 'sessions': [{'session_id': 24, 'partner_name': 'chats vi', 'client_name': 'Ggfd Bbdh', 'date': '2017-06-07T00:00:30.96'}]}})
    httpBackend.flush()
    scope.loadMore()
    expect(scope.searchObject.start_index).toBe(49)
    expect(scope.searchObject.end_index).toBe(150)
  })

  // it('ensure scrollTableUp moves table up', function () {
  //   scope.scrollTableUp()
  //   console.log()
  //   expect($('.data_sessions_table').scrollTop()).toBe(0)
  // })
})

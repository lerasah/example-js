example.controller('MarketPopupCtrl', function ($scope, market, Page, AdminService, ngDialog) { // eslint-disable-line no-undef
  $scope.market = market
  if (market.id !== 0) {
  	market.session_fee = parseFloat((market.session_fee / 100).toFixed(2))
    market.booking_fee = parseFloat((market.booking_fee / 100).toFixed(2))
    market.cancellation_fee = parseFloat((market.cancellation_fee / 100).toFixed(2))
    market.no_gym_fee = parseFloat((market.no_gym_fee / 100).toFixed(2))
  }

  $scope.saving = false

  $scope.save = function (market, frm) {
  	if (frm.$valid) {
  		consolelog('Form valid')
  		// ngDialog.close()
  		$scope.saving = true
  		if ($scope.market.id === 0) {
	        AdminService.addMarket(JSON.parse(angular.toJson($scope.market)), function (response) { // eslint-disable-line no-undef
		      $scope.saving = false
		      if (response === false) {
		        consolelog('addMarket Error')
		      } else if (Page.checkResponse(response)) {
		        consolelog('addMarket Success')
		        ngDialog.close()
		      } else if (response.status === 200 && response.data.Code === 401) {
		        // Given name already exists with the market typ
		        frm.name.$invalid = true
		        Page.commonAlert(response.data.Data, response.data.Result)
		      } else {
		        consolelog('addMarket API Returned error')
		        Page.commonAlert(response.data.Data, response.data.Result)
		      }
		    })
  		} else {
  			AdminService.editMarket(JSON.parse(angular.toJson($scope.market)), function (response) { // eslint-disable-line no-undef
		      $scope.saving = false
		      if (response === false) {
		        consolelog('editMarket Error')
		      } else if (Page.checkResponse(response)) {
		        consolelog('editMarket Success')
		        ngDialog.close()
		      } else if (response.status === 200 && response.data.Code === 401) {
		        // Given name already exists with the market typ
		        frm.name.$invalid = true
		        Page.commonAlert(response.data.Data, response.data.Result)
		      } else {
		        consolelog('editMarket API Returned error')
		        Page.commonAlert(response.data.Data, response.data.Result)
		      }
		    })
  		}
  	}
  }
})

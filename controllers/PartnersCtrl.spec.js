/*
* @Author: sachethana
* @Date:   2018-02-19 14:39:13
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:29:57
*/
describe('PartnersCtrl Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope

  beforeEach(inject(function ($rootScope, $cookies, Page, AdminService, md5, $controller, $timeout, $httpBackend) {
    scope = $rootScope.$new()
    controller = $controller('PartnersCtrl', {
      $scope: scope,
      Page: Page
    })
    httpBackend = $httpBackend
    pageCtrl = Page
  }))

  it('sortTypes exists', function () {
    expect(scope.sortTypes).toBeDefined()
  })

  it('flags exists', function () {
    expect(scope.hasMoreData).toBeDefined()
    expect(scope.itemsPerPage).toBeDefined()
    expect(scope.searchOnProgress).toBeDefined()
  })

  it('searchObject in correct format', function () {
    expect(scope.searchObject.sort_type).toEqual(jasmine.any(Number))
    expect(scope.searchObject.start_index).toEqual(jasmine.any(Number))
    expect(scope.searchObject.end_index).toEqual(jasmine.any(Number))
    expect(scope.searchObject.search_string).toEqual(jasmine.any(String))
  })

  it('Initial Search Test', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(49)
    expect(scope.searchObject.end_index).toBe(100)
  })

  it('keyPressed event for enter key', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})
    httpBackend.flush()
    done()
    scope.keyPressed({keyCode: 13})
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
  })

  it('changeSort changes the search object sort_type', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})
    httpBackend.flush()
    done()
    expect(scope.searchObject.sort_type).toBe(2)
    scope.changeSort(4)
    expect(scope.searchObject.sort_type).toBe(4)
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
  })

  it('changeSort changes the search object sort_type to same type (no change)', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})
    httpBackend.flush()
    done()
    expect(scope.searchObject.sort_type).toBe(2)
    scope.changeSort(2)
    expect(scope.searchObject.sort_type).toBe(2)
    expect(scope.searchObject.start_index).toBe(49)
    expect(scope.searchObject.end_index).toBe(100)
  })

  it('ensure loadMore searches again', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})
    httpBackend.flush()
    done()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(49)
    expect(scope.searchObject.end_index).toBe(100)
    scope.loadMore()
  })

  it('Search Returns No More Data Flag', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 201, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': []}})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
    expect(scope.hasMoreData).not.toBeTruthy()
  })

  it('Search Returns an API Error (500.etc)', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 401, 'Result': 'Unauthorized', 'Data': {'Message': 'API ERROR'}})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
    expect(scope.partners.length).toBe(0)
  })

  it('Search is Unauthorized', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond(401, {'Code': 401, 'Result': 'Unauthorized', 'Data': null})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.searchObject.start_index).toBe(0)
    expect(scope.searchObject.end_index).toBe(50)
    expect(scope.partners.length).toBe(0)
  })

  it('Load more button', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    expect(scope.searchOnProgress).not.toBeTruthy()
    scope.loadMore()
    expect(scope.partners.length).toBe(1)
  })

  it('Search avoids load more from firing when clicked rapidly', function (done) {
    httpBackend.when('POST', apiUrl + 'partners').respond({'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 49, 'response_data': [{'client_id': 225, 'full_name': 'Firstname LastName', 'email': 'validemail2@test.com', 'joined_at': '2017-05-18T11:31:22.42', 'status': 'INACTIVE'}]}})

    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    scope.executeSearch()
    expect(scope.searchOnProgress).toBeTruthy()
    scope.loadMore()
    expect(scope.partners.length).toBe(1)
  })
})

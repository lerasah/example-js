/*
* @Author: sachethana
* @Date:   2018-02-19 16:34:54
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:27
*/
describe('Page Service Tests', function () {
  beforeEach(function () {
    module('example')
  })

  it('get/set bodyClass', inject(function (Page) {
    Page.setBodyClass('body class')
    expect(Page.bodyClass()).toBe('body class')
  }))

  it('getPath redirect', inject(function (Page) {
    expect(Page.getPath()).toBe('')
    Page.redirect('/login')
    expect(Page.getPath()).toBe('/login')
  }))

  it('get/set title', inject(function (Page) {
    Page.setTitle('Demo')
    expect(Page.title()).toBe('Demo')
    expect(document.title).toBe('Example Dashboard - Demo')
  }))

  it('get/set header', inject(function (Page) {
    Page.setHeader('/partials/header.html')
    expect(Page.headerUrl()).toBe('/partials/header.html')
  }))

  it('get/set footer', inject(function (Page) {
    Page.setFooter('/partials/footer.html')
    expect(Page.footerUrl()).toBe('/partials/footer.html')
  }))

  it('checkResponse 200', inject(function (Page) {
    expect(Page.checkResponse({
      status: 200,
      data: {
        Code: 200
      }
    })).toBe(true)

    expect(Page.checkResponse({
      status: 200,
      data: {
        Code: 401
      }
    })).not.toBe(true)
  }))

  it('checkResponse 200', inject(function (Page) {
    expect(Page.checkResponse({
      status: 200,
      data: {
        Code: 200
      }
    })).toBe(true)

    expect(Page.checkResponse({
      status: 200,
      data: {
        Code: 401
      }
    })).not.toBe(true)
  }))
})

example.factory('Page', ['$location', 'ngDialog', '$cookies', '$document', function ($location, ngDialog, $cookies, $document) { // eslint-disable-line no-undef
  var title = siteTitle // eslint-disable-line no-undef
  var headerUrl = 'partials/header.html'
  var footerUrl = 'partials/footer.html'
  var bodyClass = ''
  return {
    getPath: function () {
      return $location.path()
    },
    bodyClass: function () {
      return bodyClass
    },
    setBodyClass: function (cls) {
      bodyClass = cls
    },
    title: function () {
      return title
    },
    setTitle: function (newTitle) {
      title = newTitle
      $(document).attr('title', siteTitle + ' - ' + newTitle) // eslint-disable-line no-undef
    },
    headerUrl: function () {
      return headerUrl
    },
    setHeader: function (url) {
      headerUrl = url
    },
    footerUrl: function () {
      return footerUrl
    },
    setFooter: function (url) {
      footerUrl = url
    },
    checkResponse: function (response) {
      return response.status === 200 && response.data.Code === 200
    },
    checkErrorResponse: function (response) {
      if (response.status === 401) {
        ngDialog.closeAll()
        this.commonAlert('You need to login again', 'Oops!')
        this.logout()
      } else {
        if (response.data.Data !== undefined) {
          this.commonAlert(response.data.Data.Message !== undefined ? response.data.Data.Message : 'Server Error', response.data.Result !== undefined ? response.data.Data.Result : 'Error!')
        } else {
          this.commonAlert('Server Error', 'Error!')
        }
      }
    },
    getError: function (response) {
      return response.data.Data.Message === undefined ? response.data.Data : response.data.Data.Message
    },
    redirect: function (dest, stopScroll) {
      $location.hash('')
      $location.path(dest)
      if (!stopScroll) $document.scrollTop(0, 500)
    },
    logout: function (path) {
      var cookies = $cookies.getAll()
      angular.forEach(cookies, function (v, k) { // eslint-disable-line no-undef
        $cookies.remove(k)
      })
      consolelog((path === undefined ? '/login' : path)) // eslint-disable-line no-undef
      $location.path((path === undefined ? '/login' : path))
      $document.scrollTop(0, 500)
    },
    isLoggedIn: function () {
      return $cookies.get('token') !== undefined
    },
    commonAlert: function (text, header) {
      ngDialog.open({
        template: 'partials/common_alert_view.html',
        className: 'ngdialog-theme-example info',
        resolve: {
          message: function () {
            return text
          },
          header: function () {
            if (header === undefined) {
              return 'Alert'
            } else {
              return header
            }
          }
        },
        controller: ['$scope', 'message', 'header', function ($scope, message, header) {
          $scope.message = message
          $scope.header = header
        }]
      })
    },
    commonConfirm: function (text, header, yes, no, callback) {
      ngDialog.openConfirm({
        template: 'partials/common_confirm_view.html',
        className: 'ngdialog-theme-example info',
        resolve: {
          message: function () {
            if (text === undefined) {
              return 'Are you sure?'
            } else {
              return text
            }
          },
          header: function () {
            if (header === undefined) {
              return 'Confirm Action'
            } else {
              return header
            }
          },
          yes: function () {
            if (header === undefined) {
              return 'Yes'
            } else {
              return yes
            }
          },
          no: function () {
            if (header === undefined) {
              return 'No'
            } else {
              return no
            }
          }
        },
        controller: function ($scope, message, header, yes, no) {
          $scope.message = message
          $scope.header = header
          $scope.yes = yes
          $scope.no = no
        }
      }).then(function (value) {
        callback(true)
      }, function (value) {
        callback(false)
      })
    }
  }
}])

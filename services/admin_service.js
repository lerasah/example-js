example.factory('AdminService', ['$http', '$cookies', 'Page', function ($http, $cookies, Page) {
  return {
    signin: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'signin',
        headers: {
          'Content-Type': 'application/json'
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        callback(false)
      })
    },
    verifyPin: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'verifyPin',
        headers: {
          'Content-Type': 'application/json'
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        callback(false)
      })
    },
    searchSessions: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'sessions',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    getSession: function (session_id, callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'sessions/' + session_id,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    searchClients: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'clients',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    getClient: function (client_id, callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'client/' + client_id + '/basic',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    editClient: function (clientObj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'clients/edit',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: clientObj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    loginHistory: function (type, userid, start_index, end_index, callback) {
      $http({
        method: 'GET',
        url: apiUrl + type + '/' + userid + '/login-history/' + start_index + '/to/' + end_index + '/',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        callback(false)
      })
    },
    sessionHistory: function (type, userid, start_index, end_index, callback) {
      $http({
        method: 'GET',
        url: apiUrl + type + '/' + userid + '/session-history/' + start_index + '/to/' + end_index + '/',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        callback(false)
      })
    },
    searchPartners: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'partners',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    getPartner: function (partner_id, callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'partner/' + partner_id + '/basic',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    getPartnerWorkoutTypes: function (partner_id, callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'partner/' + partner_id + '/workout-types',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        callback(false)
      })
    },
    editPartner: function (partnerObj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'partners/edit',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: partnerObj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    loadMarkets: function (callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'markets',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    addMarket: function (obj, callback) {
      obj.session_fee = obj.session_fee * 100
      obj.booking_fee = obj.booking_fee * 100
      obj.cancellation_fee = obj.cancellation_fee * 100
      obj.no_membership_fee = obj.no_gym_fee * 100 // inconsistent
      obj.no_gym_fee = obj.no_membership_fee // inconsistent
      $http({
        method: 'POST',
        url: apiUrl + 'market',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    editMarket: function (obj, callback) {
      obj.session_fee = obj.session_fee * 100
      obj.booking_fee = obj.booking_fee * 100
      obj.cancellation_fee = obj.cancellation_fee * 100
      obj.no_membership_fee = obj.no_gym_fee * 100 // inconsistent
      obj.no_gym_fee = obj.no_membership_fee // inconsistent
      $http({
        method: 'POST',
        url: apiUrl + 'markets/edit',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    deleteMarket: function (market_id, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'markets/' + (market_id).toFixed() + '/remove',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    searchAreas: function (searchString, callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'serviceArea',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        params: {
          searchName: searchString || '%'
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    deleteArea: function (id, callback) {
      $http({
        method: 'DELETE',
        url: apiUrl + 'serviceArea/' + (id).toFixed() + '/',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    saveArea: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'serviceArea' + (obj.id !== 0 ? '/edit' : ''),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    getQuestions: function (callback) {
      $http({
        method: 'GET',
        url: apiUrl + 'questions',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        callback(false)
      })
    },
    deleteQuestion: function (id, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'questions/' + (id).toFixed() + '/remove',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        }
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    },
    saveQuestion: function (obj, callback) {
      $http({
        method: 'POST',
        url: apiUrl + 'questions' + (obj.id !== 0 ? '/edit' : '/create'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': $cookies.get('token') !== undefined ? $cookies.get('token') : ''
        },
        data: obj
      }).then(function successCallback (response) {
        callback(response)
      }, function errorCallback (response) {
        Page.checkErrorResponse(response)
        callback(false)
      })
    }
  }
}])

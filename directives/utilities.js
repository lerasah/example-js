// 1e32 is enogh for working with 32-bit
// 1e8 for 8-bit (100000000)
// in your case 1e4 (aka 10000) should do it
example.filter('numberFixedLen', function () {
  return function (a, b) {
    return (1e4 + a + '').slice(-b)
  }
})

example.filter('centsToDollars', function () {
  return function (input) {
    if (input === '' || input == null || input == undefined) {
      return 0
    }
    return parseInt(input, 10) / 100
  }
})

example.filter('formatPhoneNumber', function () {
  return function (number) {
    if (!number) {
      return ''
    }
    var value = number.toString().trim().replace(/\D/g, '')
    if (value.length < 10) {
      return ''
    }
    return value.substring(value.length - 4 - 3 - 3, value.length - 4 - 3) + '-' + value.substring(value.length - 4 - 3, value.length - 4) + '-' + value.substring(value.length - 4, value.length)
  }
})

example.filter('ssnNo', function () {
  return function (ssn) {
    if (!ssn) {
      return ''
    }
    var value = ssn.toString().trim().replace(/\D/g, '')
    var returnNumber = ''
    switch (value.length) {
      case 1:
        returnNumber = 'X'; break
      case 2:
        returnNumber = 'XX'; break
      case 3:
        returnNumber = 'XXX'; break
      case 4:
        returnNumber = 'XXX-X'; break
      case 5:
        returnNumber = 'XXX-XX'; break
      case 6:
        returnNumber = 'XXX-XX-' + value.slice(5, 6); break
      case 7:
        returnNumber = 'XXX-XX-' + value.slice(5, 7); break
      case 8:
        returnNumber = 'XXX-XX-' + value.slice(5, 8); break
      case 9:
        returnNumber = 'XXX-XX-' + value.slice(5, 9); break
        break
      default:
        returnNumber = 'XXX-XX-' + value.slice(5, 9)
    }
    return returnNumber
  }
})

app.directive('includeReplace', function () {
  return {
    require: 'ngInclude',
    restrict: 'A', /* optional */
    link: function (scope, el, attrs) {
      el.replaceWith(el.children())
    }
  }
})

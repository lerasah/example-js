/*
* @Author: sachethana
* @Date:   2018-02-22 22:34:32
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:21
*/
describe('loginHistory [Directive] Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope, element, $scope
  var firstResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 5, 'response_data': [{'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-06T11:03:02.33', 'device': 'ANDROID'}, {'date': '2018-02-05T07:09:37.80', 'device': 'iOS'}, {'date': '2018-01-26T10:49:12.33', 'device': 'iOS'}, {'date': '2018-01-26T09:44:15.07', 'device': 'iOS'}]}}
  var secondResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 5, 'end_index': 10, 'response_data': [{'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-06T11:03:02.33', 'device': 'ANDROID'}, {'date': '2018-02-05T07:09:37.80', 'device': 'iOS'}, {'date': '2018-01-26T10:49:12.33', 'device': 'iOS'}, {'date': '2018-01-26T09:44:15.07', 'device': 'iOS'}]}}

  beforeEach(inject(function ($rootScope, $compile, $httpBackend) {
    scope = $rootScope.$new()
    httpBackend = $httpBackend
    httpBackend.when('GET', apiUrl + 'client/263/login-history/0/to/5/').respond(firstResponse)
    element = '<login-history userid="263" type="client" count="10" initial="5"></login-history>'
    element = $compile(element)(scope)
    scope.$digest()
  }))

  it('flags exists', function () {
    var isolated = element.isolateScope()
    expect(isolated.hasMoreData).toBeDefined()
    expect(isolated.itemsPerPage).toBeDefined()
    expect(isolated.searchOnProgress).toBeDefined()
  })

  it('search object in correct format', function () {
    var isolated = element.isolateScope()
    expect(isolated.searchObject.startIndex).toEqual(jasmine.any(Number))
    expect(isolated.searchObject.endIndex).toEqual(jasmine.any(Number))
  })

  it('passed arguments are in correct format', function () {
    var isolated = element.isolateScope()
    expect(isolated.userid).toEqual(jasmine.any(Number))
    expect(isolated.type).toEqual(jasmine.any(String))
    expect(isolated.count).toEqual(jasmine.any(Number))
    expect(isolated.initial).toEqual(jasmine.any(Number))
  })

  it('passed arguments are in correct and as expected', function () {
    var isolated = element.isolateScope()
    expect(isolated.userid).toEqual(263)
    expect(isolated.type).toEqual('client')
    expect(isolated.count).toEqual(10)
    expect(isolated.initial).toEqual(5)
  })

  it('Initial Search Test', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/login-history/0/to/5/').respond(firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.loginHistory.length).toBe(5)
  })

  it('ensure loadMore searches again', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/login-history/0/to/5/').respond(firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.loginHistory.length).toBe(5)
    isolated.searchOnProgress = false
    httpBackend.when('GET', apiUrl + 'client/263/login-history/5/to/15/').respond(secondResponse)
    done()
    httpBackend.flush()
    isolated.loadMore()
  })
})

describe('loginHistory [Directive] Tests - bad Endpoint', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope, element, $scope
  var firstResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 5, 'response_data': [{'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-06T11:03:02.33', 'device': 'ANDROID'}, {'date': '2018-02-05T07:09:37.80', 'device': 'iOS'}, {'date': '2018-01-26T10:49:12.33', 'device': 'iOS'}, {'date': '2018-01-26T09:44:15.07', 'device': 'iOS'}]}}

  beforeEach(inject(function ($rootScope, $compile, $httpBackend) {
    scope = $rootScope.$new()
    httpBackend = $httpBackend
    httpBackend.when('GET', apiUrl + 'client/263/login-history/0/to/5/').respond(401, firstResponse)
    element = '<login-history userid="263" type="client" count="10" initial="5"></login-history>'
    element = $compile(element)(scope)
    scope.$digest()
  }))

  it('login history api returns error', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/login-history/0/to/5/').respond(401, firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.loginHistory.length).toBe(0)
  })
})

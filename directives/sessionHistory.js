/*
* @Author: sachethana
* @Date:   2018-02-22 18:38:40
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:30:23
*/
example.directive('sessionHistory', function () { // eslint-disable-line no-undef
  return {
    restrict: 'E',
    scope: {
      userid: '=',
      type: '@',
      initial: '=',
      count: '='
    },
    replace: true,
    templateUrl: 'partials/session_history.html',
    controller: function ($scope, AdminService, Page) {
      $scope.page = Page
      consolelog($scope.userid)
      consolelog($scope.type)
      consolelog($scope.count)
      consolelog($scope.initial)

      $scope.sessionHistory = []

      $scope.hasMoreData = true
      $scope.itemsPerPage = $scope.initial
      $scope.searchObject = {
        startIndex: 0,
        endIndex: $scope.itemsPerPage
      }

      $scope.searchOnProgress = false

      $scope.executeSearch = function () {
        $scope.searchOnProgress = true
        AdminService.sessionHistory($scope.type, $scope.userid, $scope.searchObject.startIndex, $scope.searchObject.endIndex, function (response) { // eslint-disable-line no-undef
          $scope.searchOnProgress = false
          if (response === false) {
            consolelog('sessionHistory Error')
          } else if (Page.checkResponse(response)) {
            consolelog('sessionHistory Success')
            $scope.hasMoreData = response.data.Data.response_data.length === $scope.itemsPerPage
            if ($scope.searchObject.startIndex === 0) {
              $scope.itemsPerPage = $scope.count
              $scope.sessionHistory = []
            }
            $scope.sessionHistory = $scope.sessionHistory.concat(response.data.Data.response_data)
            $scope.searchObject.startIndex = response.data.Data.end_index
            $scope.searchObject.endIndex = $scope.searchObject.endIndex + $scope.itemsPerPage
          } else if (response.status === 200 && response.data.Code === 201) {
            // no more results
            $scope.hasMoreData = false
          } else {
            consolelog('sessionHistory API Returned error')
            if ($scope.searchObject.startIndex !== 0) {
              Page.commonAlert(response.data.Data.Message, response.data.Result)
            }
          }
        })
      }
      $scope.executeSearch()

      $scope.loadMore = function () {
        if (!$scope.searchOnProgress) {
          $scope.executeSearch()
        }
      }
    }
  }
})

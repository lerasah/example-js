/*
* @Author: sachethana
* @Date:   2018-02-22 22:34:32
* @Last Modified by:   Hasarel
* @Last Modified time: 2018-04-17 23:38:45
*/
describe('sessionHistory [Directive] Tests', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope, element, $scope
  var firstResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 5, 'response_data': [{'date': '2018-02-05T08:03:00.00', 'session_id': 913, 'partner_name': 'Example Partner'}, {'date': '2018-02-05T08:03:00.00', 'session_id': 912, 'partner_name': 'Example Partner'}, {'date': '2018-01-26T10:43:00.00', 'session_id': 911, 'partner_name': 'Example Partner'}, {'date': '2018-01-26T08:43:00.00', 'session_id': 910, 'partner_name': 'Example Partner'}, {'date': '2018-01-26T08:38:00.00', 'session_id': 909, 'partner_name': 'Example Partner'}]}}
  var secondResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 5, 'end_index': 15, 'response_data': [{'date': '2018-01-26T08:53:00.00', 'session_id': 908, 'partner_name': 'Example Partner'}, {'date': '2018-01-26T08:18:00.00', 'session_id': 907, 'partner_name': 'Example Partner'}, {'date': '2018-01-26T08:18:00.00', 'session_id': 906, 'partner_name': 'Example Partner'}, {'date': '2018-01-26T06:08:00.00', 'session_id': 905, 'partner_name': 'Example Partner'}, {'date': '2018-01-25T10:53:00.00', 'session_id': 904, 'partner_name': 'Example Partner'}, {'date': '2018-01-25T06:28:00.00', 'session_id': 903, 'partner_name': 'Example Partner'}, {'date': '2018-01-25T06:28:00.00', 'session_id': 902, 'partner_name': 'Example Partner'}, {'date': '2018-01-25T06:08:00.00', 'session_id': 901, 'partner_name': 'Example Partner'}, {'date': '2018-01-24T10:23:00.00', 'session_id': 900, 'partner_name': 'Test Partner'}, {'date': '2018-01-24T10:23:00.00', 'session_id': 899, 'partner_name': 'Test Partner'}]}}

  beforeEach(inject(function ($rootScope, $compile, $httpBackend) {
    scope = $rootScope.$new()
    httpBackend = $httpBackend
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(firstResponse)
    element = '<session-history userid="263" type="client" count="10" initial="5"></session-history>'
    element = $compile(element)(scope)
    scope.$digest()
  }))

  it('flags exists', function () {
    var isolated = element.isolateScope()
    expect(isolated.hasMoreData).toBeDefined()
    expect(isolated.itemsPerPage).toBeDefined()
    expect(isolated.searchOnProgress).toBeDefined()
  })

  it('search object in correct format', function () {
    var isolated = element.isolateScope()
    expect(isolated.searchObject.startIndex).toEqual(jasmine.any(Number))
    expect(isolated.searchObject.endIndex).toEqual(jasmine.any(Number))
  })

  it('passed arguments are in correct format', function () {
    var isolated = element.isolateScope()
    expect(isolated.userid).toEqual(jasmine.any(Number))
    expect(isolated.type).toEqual(jasmine.any(String))
    expect(isolated.count).toEqual(jasmine.any(Number))
    expect(isolated.initial).toEqual(jasmine.any(Number))
  })

  it('passed arguments are in correct and as expected', function () {
    var isolated = element.isolateScope()
    expect(isolated.userid).toEqual(263)
    expect(isolated.type).toEqual('client')
    expect(isolated.count).toEqual(10)
    expect(isolated.initial).toEqual(5)
  })

  it('Initial Search Test', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.sessionHistory.length).toBe(5)
  })

  it('ensure loadMore searches again', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.sessionHistory.length).toBe(5)
    isolated.searchOnProgress = false
    httpBackend.when('GET', apiUrl + 'client/263/session-history/5/to/15/').respond(secondResponse)
    done()
    httpBackend.flush()
    isolated.loadMore()
  })
})

describe('sessionHistory [Directive] Tests - bad Endpoint', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope, element, $scope
  var firstResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 5, 'response_data': [{'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-06T11:03:02.33', 'device': 'ANDROID'}, {'date': '2018-02-05T07:09:37.80', 'device': 'iOS'}, {'date': '2018-01-26T10:49:12.33', 'device': 'iOS'}, {'date': '2018-01-26T09:44:15.07', 'device': 'iOS'}]}}

  beforeEach(inject(function ($rootScope, $compile, $httpBackend) {
    scope = $rootScope.$new()
    httpBackend = $httpBackend
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(401, firstResponse)
    element = '<session-history userid="263" type="client" count="10" initial="5"></session-history>'
    element = $compile(element)(scope)
    scope.$digest()
  }))

  it('session history api returns error', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(401, firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.sessionHistory.length).toBe(0)
  })
})

describe('sessionHistory [Directive] Tests - no more data', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope, element, $scope
  var firstResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 5, 'response_data': [{'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-06T11:03:02.33', 'device': 'ANDROID'}, {'date': '2018-02-05T07:09:37.80', 'device': 'iOS'}, {'date': '2018-01-26T10:49:12.33', 'device': 'iOS'}, {'date': '2018-01-26T09:44:15.07', 'device': 'iOS'}]}}

  beforeEach(inject(function ($rootScope, $compile, $httpBackend) {
    scope = $rootScope.$new()
    httpBackend = $httpBackend
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(200, {'Code': 201, 'Result': 'Success', 'Data': {'start_index': 5, 'end_index': 15, 'response_data': []}})
    element = '<session-history userid="263" type="client" count="10" initial="5"></session-history>'
    element = $compile(element)(scope)
    scope.$digest()
  }))

  it('no more LOAD MORE button after 201', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(401, firstResponse)
    done()
    // console.log(scope.signinObj);
    httpBackend.flush()
    var isolated = element.isolateScope()
    isolated.executeSearch()
    expect(isolated.sessionHistory.length).toBe(0)
    expect(isolated.hasMoreData).not.toBeTruthy()
  })
})

describe('sessionHistory [Directive] Tests - unknown error', function () { // describe your object type
  beforeEach(module('example'))
  beforeEach(module('ngTemplates'))

  var controller, AdminService, pageCtrl, httpBackend, scope, element, $scope
  var firstResponse = {'Code': 200, 'Result': 'Success', 'Data': {'start_index': 0, 'end_index': 5, 'response_data': [{'date': '2018-02-09T06:13:14.66', 'device': 'iOS'}, {'date': '2018-02-06T11:03:02.33', 'device': 'ANDROID'}, {'date': '2018-02-05T07:09:37.80', 'device': 'iOS'}, {'date': '2018-01-26T10:49:12.33', 'device': 'iOS'}, {'date': '2018-01-26T09:44:15.07', 'device': 'iOS'}]}}

  beforeEach(inject(function ($rootScope, $compile, $httpBackend) {
    scope = $rootScope.$new()
    httpBackend = $httpBackend
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(500, {'Code': 201, 'Result': 'Success', 'Data': 'Error!'})
    element = '<session-history userid="263" type="client" count="10" initial="5"></session-history>'
    element = $compile(element)(scope)
    scope.$digest()
  }))

  it('no more LOAD MORE button after 201', function (done) {
    httpBackend.when('GET', apiUrl + 'client/263/session-history/0/to/5/').respond(401, firstResponse)
    done()
    var isolated = element.isolateScope()
    expect(isolated.sessionHistory.length).toBe(0)
  })
})

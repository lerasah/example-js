### Prerequisite

`npm`

### Setup

set the `activeConfig` variable to the desired mode in js/config.js.

set the `mapsApiKey` and `apiUrl` for the desired configuration.

`nvm use 5.0`

`npm install`

`npm install http-server -g`

`http-server`

### Test

`npm install -g karma-cli`

`karma start`
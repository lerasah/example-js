var example = angular.module('example', ['ngRoute', 'ngDialog', 'ngCookies', 'angular-md5', 'ngSanitize', 'angular-momentjs', 'duScroll', 'chart.js', 'ngMask', 'ngMap', 'vsGoogleAutocomplete', 'xeditable', 'ui.bootstrap'])
var config = {
  'dev': {
    apiUrl: 'https://exampleapidev.azurewebsites.net/api/v1/dashboard/',
    debugEnabled: true,
    mapsApiURL: 'https://maps.google.com/maps/api/js',
    mapsApiKey: 'USE_YOUR_OWN'
  },
  'uat': {
    apiUrl: 'https://exampleapiuat.azurewebsites.net/api/v1/dashboard/',
    debugEnabled: false,
    mapsApiURL: 'https://maps.google.com/maps/api/js',
    mapsApiKey: 'USE_YOUR_OWN'
  }
}
var activeConfig = 'dev'

var apiUrl = config[activeConfig].apiUrl

var siteTitle = 'Example Dashboard'

var consolelog = function (text) {
  if (config[activeConfig].debugEnabled) {
    console.log(text)
  }
}

example.config(function ($routeProvider, $locationProvider, ngDialogProvider) {
  // $locationProvider.html5Mode(true)
  $routeProvider
    .when('/', {
      templateUrl: 'partials/blank.html',
      controller: 'HomeCtrl'
    }).when('/login', {
      templateUrl: 'partials/login.html',
      controller: 'LoginCtrl'
    }).when('/sessions', {
      templateUrl: 'partials/sessions/sessions.html',
      controller: 'SessionsCtrl'
    }).when('/sessions/:id', {
      templateUrl: 'partials/sessions/view.html',
      controller: 'SessionViewCtrl'
    }).when('/clients', {
      templateUrl: 'partials/clients/clients.html',
      controller: 'ClientsCtrl'
    }).when('/clients/:id', {
      templateUrl: 'partials/clients/view.html',
      controller: 'ClientViewCtrl'
    }).when('/partners', {
      templateUrl: 'partials/partners/partners.html',
      controller: 'PartnersCtrl'
    }).when('/partners/:id', {
      templateUrl: 'partials/partners/view.html',
      controller: 'PartnerViewCtrl'
    }).when('/markets', {
      templateUrl: 'partials/markets/markets.html',
      controller: 'MarketsCtrl'
    }).when('/areas', {
      templateUrl: 'partials/areas/areas.html',
      controller: 'AreasCtrl'
    }).when('/questionnaire', {
      templateUrl: 'partials/questions/questions.html',
      controller: 'QuestionsCtrl'
    })

  ngDialogProvider.setDefaults({
    className: 'ngdialog-theme-example',
    plain: false,
    showClose: false,
    closeByDocument: true,
    closeByEscape: true,
    appendTo: false
  })
})

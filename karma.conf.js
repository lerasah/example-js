// Karma configuration
// Generated on Mon Feb 19 2018 13:18:12 GMT+0530 (Sri Lanka Standard Time)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      './node_modules/angular/angular.min.js',
      './node_modules/angular-mocks/angular-mocks.js', // mock
      './node_modules/angular-route/angular-route.min.js',
      './node_modules/jquery/dist/jquery.min.js',
      './node_modules/bootstrap/dist/js/bootstrap.min.js',
      './node_modules/angular-md5/angular-md5.min.js',
      './node_modules/angular-sanitize/angular-sanitize.min.js',
      './node_modules/angular-cookies/angular-cookies.min.js',
      './node_modules/ng-dialog/js/ngDialog.min.js',
      './node_modules/angular-timer/dist/assets/js/angular-timer-all.min.js',
      './node_modules/angular-momentjs/angular-momentjs.min.js',
      './node_modules/angular-scroll/angular-scroll.min.js',
      './node_modules/angularjs-datepicker/src/js/angular-datepicker.js',
      './node_modules/chart.js/dist/Chart.min.js',
      './node_modules/angular-chart.js/dist/angular-chart.min.js',
      './node_modules/ng-mask/ngMask.js',
      './node_modules/ngmap/build/scripts/ng-map.min.js',
      'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDp9j5qJAoFDzgegZ6WDuJk1u9v6W_ZRPc',
      './node_modules/vsGoogleAutocomplete/dist/vs-google-autocomplete.min.js',
      './node_modules/vsGoogleAutocomplete/dist/vs-autocomplete-validator.min.js',
      './node_modules/angular-xeditable/dist/js/xeditable.js',
      'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.3.1/ui-bootstrap-tpls.min.js',
      './js/app.js',
      './services/page_context.js',
      './services/admin_service.js',
      './controllers/BaseCtrl.js',
      './controllers/HomeCtrl.js',
      './controllers/LoginCtrl.js',
      './controllers/SessionsCtrl.js',
      './controllers/SessionViewCtrl.js',
      './controllers/ClientsCtrl.js',
      './controllers/ClientViewCtrl.js',
      './controllers/PartnersCtrl.js',
      './controllers/PartnerViewCtrl.js',
      './controllers/MarketsCtrl.js',
      './controllers/MarketPopupCtrl.js',
      './controllers/QuestionsCtrl.js',
      './controllers/AreasCtrl.js',
      './directives/utilities.js',
      './directives/loginHistory.js',
      './directives/sessionHistory.js',
      // views
      './partials/*.html',
      // test files
      // './controllers/ClientViewCtrl.spec.js'
      // './controllers/PartnerViewCtrl.spec.js'
      './controllers/*.spec.js',
      './directives/*.spec.js',
      './services/*.spec.js'
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      './!(node_modules)/*.js': ['coverage'],
      '**/*.html': ['ng-html2js']
    },

    jasmineNodeOpts: {
      defaultTimeoutInterval: 2500000
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: '',
      moduleName: 'ngTemplates' // you can name this whatever you want
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    // reporters: ['progress','spec'],
    reporters: ['spec', 'coverage'],
    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
